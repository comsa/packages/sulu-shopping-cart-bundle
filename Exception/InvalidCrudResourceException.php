<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Exception;

use Throwable;

class InvalidCrudResourceException extends \Exception {
    public function __construct( string $expectedClass, string $givenClass) {
        parent::__construct(
            sprintf("Wrong class given. Expected class %s, but received class %s", $expectedClass, $givenClass),
            500
        );
    }
}
