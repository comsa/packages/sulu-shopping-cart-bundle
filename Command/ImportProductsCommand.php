<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Command;

use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Factory\XlsxFactory;
use Comsa\SuluShoppingCart\Service\DocumentService;
use Comsa\SuluShoppingCart\Service\ImportService;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\Content\Types\ResourceLocator\Strategy\ResourceLocatorStrategyPool;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;
use Sulu\Component\Webspace\Manager\WebspaceManager;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportProductsCommand extends Command
{
    protected static $defaultName = "comsa:shopping:import-products";

    private DocumentManagerInterface $documentManager;
    private EntityManagerInterface $entityManager;
    private WebspaceManagerInterface $webspaceManager;
    private ResourceLocatorStrategyPool $resourceLocatorStrategyPool;
    private ImportService $importService;
    private DocumentService $documentService;

    public function __construct(
        DocumentManagerInterface $documentManager,
        EntityManagerInterface $entityManager,
        WebspaceManagerInterface $webspaceManager,
        ResourceLocatorStrategyPool $resourceLocatorStrategyPool,
        ImportService $importService,
        DocumentService $documentService,
    ) {
        $this->documentManager = $documentManager;
        $this->entityManager = $entityManager;
        $this->webspaceManager = $webspaceManager;
        $this->resourceLocatorStrategyPool = $resourceLocatorStrategyPool;
        $this->importService = $importService;
        $this->documentService = $documentService;

        parent::__construct();
    }

    protected function configure(): void {
        $this->setDescription("Imports products from file");
        $this->addArgument("inputFile", InputArgument::REQUIRED, "Set location of the inputfile.");
        $this->addArgument("parentId", InputArgument::REQUIRED, "Where will the categories and products be stored?");
        $this->addArgument("webspace", InputArgument::REQUIRED, "For what webspace will the productpages be generated?");
        $this->addArgument("locale", InputArgument::REQUIRED, "For which locale must the pages be created?");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = new SymfonyStyle($input, $output);

        $io->title("<info>Starting import...</info>");

        $inputFileName = $input->getArgument("inputFile");
        $parentId = $input->getArgument("parentId");
        $webspace = $input->getArgument("webspace");
        $locale = $input->getArgument("locale");

        try {
            /** @var PageDocument $parentPage */
            $parentPage = $this->documentManager->find($parentId);
        } catch (DocumentManagerException $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $reader = XlsxFactory::createReader();
        $spreadSheet = $reader->load($inputFileName);
        $activeSheet = $spreadSheet->getActiveSheet();
        $rows = $activeSheet->toArray();

        //-- Get all Category and create pages for these categories
        $categories = $this->importService->checkCategories(categories: $rows);

        $output->writeln("<info>Creating Categories</info>");

        //-- Create pages for categories and store them in the array
        $categoryPages = $this->documentService->createPages(pages: $categories);
        $output->writeln("<info>Categories creation done</info>");

        $firstRow = true;

        //-- Loop all rows and create products
        $output->writeln("<info>Creating products...</info>");
        $this->importService->import(data: $rows, categoryPages:  $categoryPages);
        foreach ($rows as $product) {
            if ($firstRow) {
                $firstRow = false;
                continue;
            }

            $code = $product[$colums["code"]];
            $category = $product[$colums["category"]];
            $title = $product[$colums["title"]];
            $description = $product[$colums["description"]];
            $price = (float) str_replace('€', '', $product[$colums["price"]]);
            $unit = $product[$colums["unit"]];
            $followStock = $product[$colums["followStock"]];
            $stock = $product[$colums["stock"]];

            if (!in_array($unit, Product::UNIT_ARRAY)) {
                $output->writeln("<error>Unit not recognized for product {$title}</error>");
                return Command::FAILURE;
            } elseif (
                $code === null &&
                $category === null &&
                $title === null &&
                $description === null &&
                $price === null &&
                $unit === null &&
                $followStock === null &&
                $stock === null
            ) {
                $output->writeln("<info>Product creation done</info>");

                $output->writeln("<warning>Import succesfull!</warning>");

                return Command::SUCCESS;
            }

            $product = new Product();
            $product->setCode($code);
            $product->setParentPage($categoryPages[$category]);
            $product->setTitle($title);
            $product->setDescription($description);
            $product->setPrice($price);
            $product->setUnit($unit);
            if ($followStock === 'yes') {
                $product->setFollowStock(true);
            } else {
                $product->setFollowStock(false);
            }
            $product->setStock($stock);

            $this->entityManager->persist($product);
            $this->entityManager->flush();

            $this->createProductPage($product, $output, $webspace, $locale);

        }
        $output->writeln("<info>Product creation done</info>");

        $output->writeln("<warning>Import succesfull!</warning>");

        return Command::SUCCESS;
    }

    private function createCategoryPage(string $category, string $parent, OutputInterface $output, string $webspace, string $locale): string {
        $output->writeln("<info>Creating Category page: {$category}</info>");

        $parent = $this->documentManager->find($parent);

        /** @var PageDocument $categoryPage */
        $categoryPage = $this->documentManager->create("page");
        $categoryPage->setTitle($category);
        $categoryPage->setParent($parent);
        $categoryPage->setLocale($locale);

        $categoryPage->setResourceSegment(
            $this->resourceLocatorStrategyPool->getStrategyByWebspaceKey($webspace)->generate(
                $category, $parent->getUuid(),
                $webspace,
                $locale
            )
        );

        $categoryPage->setStructureType('default');

        $this->documentManager->persist($categoryPage, $locale);
        $this->documentManager->publish($categoryPage, $locale);
        $this->documentManager->flush();

        return $categoryPage->getUuid();
    }

    public function createProductPage(Product $product, OutputInterface $output, string $webspace, string $locale) {
        $output->writeln("<info>Creating Page {$product->getTitle()}</info>");

        /** @var PageDocument $parent */
        $parent = $this->documentManager->find($product->getParentPage());

        /** @var PageDocument $productPage */
        $productPage = $this->documentManager->create("page");
        $productPage->setTitle($product->getTitle());
        $productPage->setParent($parent);
        $productPage->setLocale($locale);

        $productPage->setResourceSegment(
            $this->resourceLocatorStrategyPool->getStrategyByWebspaceKey($webspace)->generate(
                $product->getTitle(),
                $parent->getUuid(),
                $webspace,
                $locale
            )
        );
        $productPage->setStructureType("comsa_product");

        $productPage->getStructure()->getProperty('product')->setValue($product->getId());

        $this->documentManager->persist($productPage, $locale);
        $this->documentManager->publish($productPage, $locale);
        $this->documentManager->flush();

        $product->setPageId($productPage->getUuid());
        $this->entityManager->flush();
    }
}
