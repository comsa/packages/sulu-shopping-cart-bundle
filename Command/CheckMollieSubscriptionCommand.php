<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Command;

use Comsa\SuluShoppingCart\Entity\Order;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Mollie\Api\MollieApiClient;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: "comsa:shopping:check-mollie-subscription",
    description: "Checks the status of a subscription"
)]
class CheckMollieSubscriptionCommand extends Command {

    private MollieApiClient $mollieApiClient;
    private ParameterBagInterface $parameterBag;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag) {
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->mollieApiClient = new MollieApiClient();
        $this->mollieApiClient->setApiKey($this->parameterBag->get("comsa_sulu_shopping_mollie_api_key"));

        parent::__construct();
    }

    protected function configure(): void {
        $this->setDescription("Check the status a subscription");
        $this->addArgument("orderID", InputArgument::REQUIRED, "What is the ID of the order?");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $io = new SymfonyStyle($input, $output);

        /** @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)->find($input->getArgument("orderID"));

        if (!$order) {
            $output->writeln("<error>Order not found!</error>");
            return Command::FAILURE;
        }

        $subscriptionId = $order->getPayment()->getExternalId();

        if (strpos($subscriptionId, "sub_") === false) {
            $output->writeln("<error>Order is not a subscription!</error>");
            return Command::FAILURE;
        }

        $customer = $order->getCart()->getCustomer();

        $mollieCustomer = $this->mollieApiClient->customers->get($customer->getMollieId());
        $subscription = $mollieCustomer->getSubscription($subscriptionId);

        $output->writeln("<info>ID: </info> {$subscription->id}");
        $output->writeln("<info>CustomerId: </info> {$subscription->customerId}");
        $output->writeln("<info>Interval: </info> {$subscription->interval}");
        $output->writeln("<info>Amount: </info> {$subscription->amount->value} {$subscription->amount->currency}");
        $output->writeln("<info>Times: </info> {$subscription->times}");
        $output->writeln("<info>Remaining Times: </info> {$subscription->timesRemaining}");
        $output->writeln("<info>Start Date: </info> {$subscription->startDate}");
        $output->writeln("<info>Next payment Date: </info> {$subscription->nextPaymentDate}");

        return Command::SUCCESS;
    }
}
