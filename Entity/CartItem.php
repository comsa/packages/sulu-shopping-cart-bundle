<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    InheritanceType("SINGLE_TABLE"),
    DiscriminatorColumn(name: "discr", type: Types::STRING),
    DiscriminatorMap([
        "baseCartItems" => "Comsa\SuluShoppingCart\Entity\CartItem",
        "customCartItem" => "App\Entity\CartItem"
    ]),
    Table(name: "comsa_sc_cart_items"),
    ExclusionPolicy("all")
]
class CartItem {
    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Cart", inversedBy: "items", cascade: ["remove"]),
        JoinColumn(onDelete: "CASCADE"),
        Expose()
    ]
    private ?Cart $cart;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Product", inversedBy: "cartItems"),
        Expose()
    ]
    private ?Product $product;

    #[
        Column(type: Types::STRING, nullable: true),
        Expose()
    ]
    private ?string $uuid;

    #[
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $quantity = 1;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private ?string $title;

    #[
        ManyToMany(targetEntity: "Comsa\SuluShoppingCart\Entity\Option"),
        JoinTable(name: "comsa_sc_cart_item_options"),
        JoinColumn(name: "cart_item_id", referencedColumnName: "id"),
        InverseJoinColumn(name: "option_id", referencedColumnName: "id"),
        Expose()
    ]
    private ArrayCollection|Collection|array $options;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private float $price;

    public function __construct() {
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getUuid(): ?string {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self {
        $this->uuid = $uuid;

        return $this;
    }

    public function getQuantity(): ?int {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self {
        $this->quantity = $quantity;

        return $this;
    }

    public function increaseQuantity(int $quantity): self {
        $this->quantity += $quantity;

        return $this;
    }

    public function getCart(): ?Cart {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self {
        $this->cart = $cart;

        return $this;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?float {
        return $this->price;
    }

    public function setPrice(float $price): self {
        $this->price = $price;

        return $this;
    }

    public function getTotal(): float|int {
        return $this->getQuantity() * $this->getPrice();
    }

    public function getProduct(): ?Product {
        return $this->product;
    }

    public function setProduct(?Product $product): self {
        $this->product = $product;

        return $this;
    }

    public function getOptions(): ?Collection {
        return $this->options;
    }

    public function setOptions($options): self {
        $this->options = $options;

        return $this;
    }

    public function addOption(Option $option): self {
        if (!$this->options->contains($option)) {
            $this->options->add($option);
        }

        return $this;
    }

    public function removeOption(Option $option): self {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
        }

        return $this;
    }

    public function getSingleOption(): ?Option {
        if (!$this->getOptions()->isEmpty()) {
            return $this->getOptions()->first();
        } else {
            return null;
        }
    }

    public function setSingleOption(Option $option): self {
        $this->addOption($option);

        return $this;
    }
}
