<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Ramsey\Collection\Collection;

#[
    Entity(),
    Table(name: "comsa_sc_options"),
    ExclusionPolicy("all")
]
class Option implements CrudResource {

    const RESOURCE_KEY = "options";

    const OPTION_SELECTION_TYPE_SINGLE = "SINGLE";
    const OPTION_SELECTION_TYPE_MULTIPLE = "MULTIPLE";
    const OPTION_SELECTION_TYPES = [
        self::OPTION_SELECTION_TYPE_SINGLE,
        self::OPTION_SELECTION_TYPE_MULTIPLE
    ];

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private string $title;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private float $price;

    public function getId(): ?int {
        return $this->id;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(?string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?float {
        return $this->price;
    }

    public function setPrice(?float $price): self {
        $this->price = $price;

        return $this;
    }
}
