<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\PersistentCollection;
use JetBrains\PhpStorm\ArrayShape;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Sulu\Bundle\MediaBundle\Entity\Media;
use Sulu\Bundle\MediaBundle\Entity\MediaInterface;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\DocumentManager\DocumentManager;

#[
    Entity(),
    Table(name: "comsa_sc_products"),
    ExclusionPolicy("all")
]
class Product implements CrudResource {

    const RESOURCE_KEY = "products";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private ?string $code;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $title;

    #[
        Column(type: Types::TEXT, length: 65534, nullable: true),
        Expose()
    ]
    private ?string $description;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private float $price;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private string $unit;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $followStock;

    #[
        Column(type: Types::INTEGER, nullable: true),
        Expose()
    ]
    private ?int $stock;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $timeLimitedAvailability;

    #[
        Column(type: Types::DATETIME_MUTABLE, nullable: true),
        Expose()
    ]
    private ?\DateTime $startTimeAvailability;

    #[
        Column(type: Types::DATETIME_MUTABLE, nullable: true),
        Expose()
    ]
    private ?\DateTime $endTimeAvailability;

    #[
        ManyToOne(targetEntity: "Sulu\Bundle\MediaBundle\Entity\Media"),
        Expose()
    ]
    private ?Media $thumbnail;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $parentPage;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private ?string $pageId;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private string $paymentMethodType;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private ?string $subscriptionInterval;

    #[
        Column(type: Types::INTEGER, nullable: true),
        Expose()
    ]
    private ?int $subscriptionAmount;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private ?string $optionSelectionType;


    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\TaxCategory"),
        JoinColumn(name: "tax_category_id", referencedColumnName: 'id')
    ]
    private ?TaxCategory $taxCategory;

    #[
        OneToMany(targetEntity: "Comsa\SuluShoppingCart\Entity\ProductOption", mappedBy: "product", cascade: [ "persist", "remove" ]),
        OrderBy(["position" => "ASC"])
    ]
    private Collection $options;

    #[
        OneToMany(targetEntity: "Comsa\SuluShoppingCart\Entity\CartItem", mappedBy: "product", cascade: ["remove"])
    ]
    private Collection $cartItems;


    public function __construct() {
        $this->pageId = null;
        $this->setFollowStock(false);
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(?string $code): self {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float {
        return $this->price;
    }

    public function setPrice(float $price): self {
        $this->price = $price;

        return $this;
    }

    public function getUnit(): ?string {
        return $this->unit;
    }

    public function setUnit(string $unit): self {
        $this->unit = $unit;

        return $this;
    }

    public function getFollowStock(): bool {
        return $this->followStock;
    }

    public function setFollowStock(bool $followStock): self {
        $this->followStock = $followStock;

        return $this;
    }

    public function getStock(): ?int {
        return $this->stock;
    }

    public function setStock(?int $stock): self {
        $this->stock = $stock;

        return $this;
    }

    public function isLimitedEdition(): ?bool {
        return $this->timeLimitedAvailability;
    }

    public function setTimeLimitedAvailbility(?bool $timeLimitedAvailability): self {
        if (is_null($timeLimitedAvailability)) {
            $this->timeLimitedAvailability = false;
        } else {
            $this->timeLimitedAvailability = $timeLimitedAvailability;
        }

        return $this;
    }

    public function getStartTimeAvailability(): ?\DateTime {
        return $this->startTimeAvailability;
    }

    public function setStartTimeAvailability(?\DateTime $startTimeAvailability): self {
        $this->startTimeAvailability = $startTimeAvailability;

        return $this;
    }

    public function getEndTimeAvailability(): ?\DateTime {
        return $this->endTimeAvailability;
    }

    public function setEndTimeAvailability(?\DateTime $endTimeAvailability): self {
        $this->endTimeAvailability = $endTimeAvailability;

        return $this;
    }

    public function getParentPage(): ?string {
        return $this->parentPage;
    }

    public function setParentPage(string $parentPage): self {
        $this->parentPage = $parentPage;

        return $this;
    }

    public function getPageId(): ?string {
        return $this->pageId;
    }

    public function setPageId(?string $uuid): self {
        $this->pageId = $uuid;

        return $this;
    }

    public function getPaymentMethodType(): ?string {
        return $this->paymentMethodType;
    }

    public function setPaymentMethodType(string $paymentMethodType): self {
        $this->paymentMethodType = $paymentMethodType;

        return $this;
    }

    public function getSubscriptionInterval(): ?string {
        return $this->subscriptionInterval;
    }

    public function setSubscriptionInterval(?string $subscriptionInterval): self {
        $this->subscriptionInterval = $subscriptionInterval;

        return $this;
    }

    public function getSubscriptionAmount(): ?int {
        return $this->subscriptionAmount;
    }

    public function setSubscriptionAmount(?int $subscriptionAmount): self {
        $this->subscriptionAmount = $subscriptionAmount;

        return $this;
    }

    public function getThumbnail(): ?MediaInterface {
        return $this->thumbnail;
    }

    public function setThumbnail(?MediaInterface $thumbnail): self {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getOptionSelectionType(): ?string {
        return $this->optionSelectionType;
    }

    public function setOptionSelectionType(?string $optionSelectionType): self {
        $this->optionSelectionType = $optionSelectionType;

        return $this;
    }

    public function hasOptions(): bool {
        return !$this->options->isEmpty();
    }

    /**
     * @return Collection|ProductOption[]
     */
    public function getOptions(): Collection {
        return $this->options;
    }

    public function getOptionsAsBlocks(): array {
        $array = [];

        /** @var Option $option */
        foreach ($this->getOptions() as $option) {
            $array[] = $option->__toBlock();
        }

        return $array;
    }

    public function setOptions(Collection $options): self {
        foreach ($options as $option) {
            $this->addOption($option);
        }

        return $this;
    }

    public function addOption(ProductOption $option): self {
        if (!$this->options->contains($option)) {
            $option->setProduct($this);
            $this->options->add($option);
        }

        return $this;
    }

    public function removeOption(ProductOption $option): self {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
        }

        return $this;
    }

    public function getTaxCategory(): ?TaxCategory {
        return $this->taxCategory;
    }

    public function setTaxCategory(?TaxCategory $taxCategory): self {
        $this->taxCategory = $taxCategory;

        return $this;
    }

    public function isAvailable() : bool {
        if ($this->isLimitedEdition()) {
            $date = new \DateTime();

            if ($date < $this->getStartTimeAvailability() || $date > $this->getEndTimeAvailability()) {
                return false;
            }
        }

        if ($this->getFollowStock()) {
            if ($this->getStock() === 0) {
                return false;
            }
        }

        return true;
    }

    public function __toArray(): array {
        return [
            "code" => $this->getCode(),
            "description" => $this->getDescription(),
            "endTimeAvailability" => $this->getEndTimeAvailability(),
            "followStock" => $this->getFollowStock(),
            "id" => $this->getId(),
            "optionSelectionType" => $this->getOptionSelectionType(),
            "options" => $this->getOptions(),
            "productOptions" => $this->getOptionsAsBlocks(),
            "pageId" => $this->getPageId(),
            "parentPage" => $this->getParentPage(),
            "paymentMethodType" => $this->getPaymentMethodType(),
            "price" => $this->getPrice(),
            "startTimeAvailability" => $this->getStartTimeAvailability(),
            "stock" => $this->getStock(),
            "subscriptionAmount" => $this->getSubscriptionAmount(),
            "subscriptionInterval" => $this->getSubscriptionInterval(),
            "thumbnail" => $this->getThumbnail(),
            "timeLimitedAvailability" => $this->isLimitedEdition(),
            "title" => $this->getTitle(),
            "unit" => $this->getUnit(),
            "taxCategory" => $this->getTaxCategory()
        ];
    }
}
