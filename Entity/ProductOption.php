<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_sc_product_options"),
    ExclusionPolicy("all")
]
class ProductOption {
    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Product", inversedBy: "options"),
        JoinColumn(name: "product_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Product $product;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Option"),
        JoinColumn(name: "option_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Option $option;

    #[
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $position;

    public function getId(): ?int {
        return $this->id;
    }

    public function getProduct(): ?Product {
        return $this->product;
    }

    public function setProduct(?Product $product): self {
        $this->product = $product;

        return $this;
    }

    public function getOption(): ?Option {
        return $this->option;
    }

    public function setOption(Option $option): self {
        $this->option = $option;

        return $this;
    }

    public function getPosition(): ?int {
        return $this->position;
    }

    public function setPosition(int $position): self {
        $this->position = $position;

        return $this;
    }

    public function __toBlock() {
        return [
            "type" => "product_option",
            "optionTitle" => $this->getOption()->getTitle(),
            "optionPrice" => TypeConverter::floatToString($this->getOption()->getPrice()),
            "optionPosition" => $this->getPosition()
        ];
    }

    public function __toString(): string {
        return sprintf("%s%d", $this->getOption()->getTitle(), $this->getOption()->getPrice());
    }
}
