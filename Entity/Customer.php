<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_sc_customers"),
    HasLifecycleCallbacks(),
    ExclusionPolicy("all")
]
class Customer {
    use TimestampableTrait;

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private string $email;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private string $firstName;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private string $lastName;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private string $phoneNumber;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Address", cascade: ["persist"]),
        JoinColumn(name: "billing_address_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Address $billingAddress;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Address", cascade: ["persist"]),
        JoinColumn(name: "shipping_address_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Address $shippingAddress;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private string $mollieId;

    public function __construct() {
        $this->id = null;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): ?string {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFullName(): ?string {
        return  $this->firstName . " " . $this->lastName;
    }

    public function getPhoneNumber(): ?string {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getBillingAddress(): ?Address {
        return $this->billingAddress;
    }

    public function setBillingAddress(?Address $billingAddress): self {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getShippingAddress(): ?Address {
        return $this->shippingAddress;
    }

    public function setShippingAddress(?Address $shippingAddress): self {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getMollieId(): ?string {
        return $this->mollieId;
    }

    public function setMollieId(string $mollieId): self {
        $this->mollieId = $mollieId;

        return $this;
    }
}
