<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Traits\TimestampableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\VirtualProperty;

#[
    Entity(),
    InheritanceType("SINGLE_TABLE"),
    DiscriminatorColumn(name: "discr", type: Types::STRING),
    DiscriminatorMap([
        "baseOrder" => "Comsa\SuluShoppingCart\Entity\Order",
        "customOrder" => "App\Entity\Order"
    ]),
    Table(name: "comsa_sc_orders"),
    HasLifecycleCallbacks(),
    ExclusionPolicy("all")
]
class Order {

    const RESOURCE_KEY = 'orders';

    use TimestampableTrait;

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        OneToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Cart", cascade: ["persist", "remove"], inversedBy: "order"),
        JoinColumn(onDelete: "CASCADE"),
        Expose()
    ]
    private ?Cart $cart;

    #[
        Column(type: Types::DATETIME_MUTABLE, nullable: true),
        Expose()
    ]
    private ?\DateTime $deliveryDate;

    #[
        Column(type: Types::TEXT, nullable: true),
        Expose()
    ]
    private ?string $note;

    #[
        OneToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Payment", mappedBy: "order", cascade: ["persist", "remove"]),
        Expose()
    ]
    private ?Payment $payment;

    public function __construct() {
        $this->payment = null;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCart(): ?Cart {
        return $this->cart;
    }

    public function setCart($cart): self {
        $this->cart = $cart;

        return $this;
    }

    public function getPayment(): ?Payment {
        return $this->payment;
    }

    public function setPayment(Payment $payment): self {
        $this->payment = $payment;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTime {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?\DateTime $deliveryDate): self {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    public function getNote(): ?string {
        return $this->note;
    }

    public function setNote($note): self {
        $this->note = $note;

        return $this;
    }

    public function isPaid(): bool {
        return $this->getPayment()->getPaymentCompletedAt() !== null;
    }

    #[VirtualProperty(name: "total")]
    public function getTotal(): int|float {
        return $this->getCart()->getTotal();
    }
}
