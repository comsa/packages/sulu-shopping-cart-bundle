<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Traits\TimestampableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_sc_payments"),
    HasLifecycleCallbacks(),
    ExclusionPolicy("all")
]
class Payment {
    use TimestampableTrait;

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        OneToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Order", inversedBy: "payment", cascade: ["persist","remove"]),
        JoinColumn(name: "order_id", referencedColumnName: "id", onDelete: "CASCADE"),
        Expose()
    ]
    private Order $order;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\PaymentMethod"),
        JoinColumn(name: "payment_method_id"),
        Expose()
    ]
    private ?PaymentMethod $paymentMethod;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private float $amount;

    #[
        Column(type: Types::STRING, nullable: true),
        Expose()
    ]
    private ?string $externalId;

    #[
        Column(type: Types::STRING, nullable: true),
        Expose()
    ]
    private ?string $externalSubscriptionId;

    #[
        Column(type: Types::DATETIME_MUTABLE, nullable: true),
        Expose()
    ]
    private ?\DateTime $paymentCompletedAt;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private string $paymentMethodType;

    #[
        Column(type: Types::INTEGER, nullable: true),
        Expose()
    ]
    private ?int $recurringTimes;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $refunded = false;

    public function __construct() {
        $this->externalId = null;
        $this->externalSubscriptionId = null;
        $this->recurringTimes = null;
        $this->paymentCompletedAt = null;
    }

    public function getOrder(): Order {
        return $this->order;
    }

    public function getPaymentMethod(): ?PaymentMethod {
        return $this->paymentMethod;
    }

    public function setOrder($order): self {
        $this->order = $order;
        $order->setPayment($this);

        return $this;
    }

    public function setPaymentMethod($paymentMethod): self {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getAmount(): ?float {
        return $this->amount;
    }

    public function setAmount($amount): self {
        $this->amount = $amount;

        return $this;
    }

    public function getExternalId(): ?string {
        return $this->externalId;
    }

    public function setExternalId($externalId): self {
        $this->externalId = $externalId;

        return $this;
    }

    public function getExternalSubscriptionId(): ?string {
        return $this->externalSubscriptionId;
    }

    public function setExternalSubscriptionId(string $externalSubscriptionId): self {
        $this->externalSubscriptionId = $externalSubscriptionId;

        return $this;
    }

    public function getPaymentCompletedAt(): ?\DateTime {
        return $this->paymentCompletedAt;
    }

    public function setPaid(): self {
        $this->paymentCompletedAt = new \DateTime();

        return $this;
    }

    public function getPaymentMethodType(): ?string {
        return $this->paymentMethodType;
    }

    public function setPaymentMethodType(string $paymentMethodType): self {
        $this->paymentMethodType = $paymentMethodType;

        return $this;
    }

    public function getRecurringTimes(): ?int {
        return $this->recurringTimes;
    }

    public function setRecurringTimes(?string $recurringTimes): self {
        $this->recurringTimes = $recurringTimes;

        return $this;
    }

    public function isRefunded(): bool {
        return $this->refunded;
    }

    public function refund() : void {
        $this->refunded = true;
    }
}
