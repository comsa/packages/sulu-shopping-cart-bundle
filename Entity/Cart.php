<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Cassandra\Custom;
use Comsa\SuluShoppingCart\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service_locator;

#[
    Entity(),
    Table(name: "comsa_sc_carts"),
    HasLifecycleCallbacks(),
    ExclusionPolicy("all")
]
class Cart {
    use TimestampableTrait;

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        OneToMany(targetEntity: "Comsa\SuluShoppingCart\Entity\CartItem", mappedBy: "cart", cascade: ["persist", "remove"], orphanRemoval: true),
        JoinColumn(onDelete: "CASCADE"),
        Expose()
    ]
    private $items;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Customer", fetch: "EAGER"),
        JoinColumn(name: "customer_id"),
        Expose()
    ]
    private Customer $customer;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\ShippingMethod", cascade: ["persist"]),
        Expose()
    ]
    private ?ShippingMethod $shippingMethod;

    #[
        ManyToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\PaymentMethod", cascade: ["persist"]),
        Expose()
    ]
    private ?PaymentMethod $paymentMethod;

    #[
        OneToOne(targetEntity: "Comsa\SuluShoppingCart\Entity\Order", mappedBy: "cart", cascade: ["remove"]),
        JoinColumn(name: "order_id", referencedColumnName: "id", onDelete: "CASCADE"),
        Expose()
    ]
    private ?Order $order;

    public function __construct() {
        $this->id = null;
        $this->items = new ArrayCollection();
        $this->order = null;
        $this->shippingMethod = null;
        $this->paymentMethod = null;

    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getItems(): Collection {
        return $this->items;
    }

    public function removeItem(CartItem $item): self {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            $item->setCart(null);
        }

        return $this;
    }

    public function addItem(CartItem $cartItem): self {
        //-- Check if UUID already exists
        $items = $this->items->filter(function(CartItem $item) use ($cartItem) {
            return ($cartItem->getTitle() === $item->getTitle() && $cartItem->getOptions() === $item->getOptions());
        });

        if (!$items->isEmpty()) {
            //-- Add quantity
            $items->first()->increaseQuantity($cartItem->getQuantity());
        } else {
            $this->items->add($cartItem);
        }

        return $this;
    }


    public function getTotal(): float|int {
        $total = 0;
        /** @var CartItem $item */
        foreach ($this->items as $item) {
            /** @var Option $option */
            foreach ($item->getOptions() as $option) {
                $total += $option->getPrice() * $item->getQuantity();
            }
            $total += $item->getTotal();
        }

        //-- Add shipping cost if any
        if ($this->shippingMethod instanceof ShippingMethod) {
            $total += $this->shippingMethod->getPrice();
        }

        //-- Add payment cost if any
        if ($this->paymentMethod instanceof PaymentMethod) {
            $total += $this->paymentMethod->getPrice();
        }

        return $total;
    }

    public function getCustomer(): ?Customer {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): self {
        $this->customer = $customer;

        return $this;
    }

    public function getShippingMethod(): ?ShippingMethod {
        return $this->shippingMethod;
    }

    public function setShippingMethod(ShippingMethod $shippingMethod): self {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(PaymentMethod $paymentMethod): self {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getOrder(): ?Order {
        return $this->order;
    }
}
