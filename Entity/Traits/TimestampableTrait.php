<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity\Traits;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;

trait TimestampableTrait {
    #[Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $createdAt;

    #[Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $updatedAt;

    public function getCreatedAt(): ?\DateTime {
        return $this->createdAt;
    }

    #[PrePersist()]
    public function setCreatedAt(): void {
        $this->createdAt = $this->updatedAt = new \DateTime();
    }

    public function getUpdatedAt(): ?\DateTime {
        return $this->updatedAt;
    }

    #[PreUpdate()]
    public function setUpdatedAt() {
        $this->updatedAt = new \DateTime();
    }
}
