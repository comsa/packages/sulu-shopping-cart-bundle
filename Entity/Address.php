<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_sc_addresses"),
    ExclusionPolicy("all")
]
class Address {
    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $street = null;

    #[
        Column(type: Types::STRING, length: 15),
        Expose()
    ]
    private ?string $streetNumber = null;

    #[
        Column(type: Types::STRING, length: 15),
        Expose()
    ]
    private ?string $postal = null;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $city = null;

    #[
        Column(type: Types::STRING, length: 2),
        Expose()
    ]
    private ?string $country = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getStreet(): ?string {
        return $this->street;
    }

    public function setStreet(string $street): self {
        $this->street = $street;

        return $this;
    }

    public function getStreetNumber(): ?string {
        return $this->streetNumber;
    }

    public function setStreetNumber(string $streetNumber): self {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getPostal(): ?string {
        return $this->postal;
    }

    public function setPostal(string $postal): self {
        $this->postal = $postal;

        return $this;
    }

    public function getCity(): ?string {
        return $this->city;
    }

    public function setCity(string $city): self {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string {
        return $this->country;
    }

    public function setCountry(string $country): self {
        $this->country = $country;

        return $this;
    }

    public function getAddress() : string {
        return sprintf("%s %s, %s %s", $this->getStreet(), $this->getStreetNumber(), $this->getPostal(), $this->getCity());
    }
}
