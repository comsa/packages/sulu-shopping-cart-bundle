<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Sulu\Bundle\MediaBundle\Entity\MediaInterface;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\DocumentManager\DocumentManager;
#[
    Entity(),
    Table(name: "comsa_sc_settings"),
    ExclusionPolicy("all")
]
class Setting implements CrudResource {

    const RESOURCE_KEY = "settings";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private string $title;

    #[
        Column(name: "`key`", type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $key;

    #[
        Column(type: Types::TEXT, length: 65000, nullable: true),
        Expose()
    ]
    private ?string $value;

    public function getId(): ?int {
        return $this->id;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getKey(): ?string {
        return $this->key;
    }

    public function setKey(string $key): self {
        $this->key = $key;

        return $this;
    }

    public function getValue(): ?string {
        return $this->value;
    }

    public function setValue(?string $value): self {
        $this->value = $value;

        return $this;
    }
}
