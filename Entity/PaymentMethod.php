<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Entity;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_sc_payment_methods"),
    ExclusionPolicy("all")
]
class PaymentMethod implements CrudResource {

    const PAYMENT_METHOD_TYPE_SINGLE_PAYMENT = "SINGLE_PAYMENT";
    const PAYMENT_METHOD_TYPE_SUBSCRIPTION = "SUBSCRIPTION_PAYMENT";
    const PAYMENT_METHOD_TYPE_ARRAY = [
        self::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT,
        self::PAYMENT_METHOD_TYPE_SUBSCRIPTION
    ];

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private string $name;

    #[
        Column(type: Types::STRING),
        Expose()
    ]
    private string $type;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private float $price;

    #[
        Column(type: Types::BOOLEAN, options: ["default" => false])
    ]
    private bool $hidden = false;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float {
        return $this->price;
    }

    public function setPrice(float $price): self {
        $this->price = $price;

        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function isHidden(): bool {
        return $this->hidden;
    }

    public function hide(): void {
        $this->hidden = true;
    }

    public function show(): void {
        $this->hidden = false;
    }

    public function __toString(): string {
        return (string) $this->name;
    }
}
