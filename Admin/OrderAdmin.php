<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Admin;

use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Setting;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;

/**
 * Configures the Sulu Admin to include Orders
 * @package Comsa\SuluShoppingCart\Admin
 */
class OrderAdmin extends Admin {

    const ORDER_LIST_KEY = "orders";
    const ORDER_LIST_VIEW = "comsa_sulu_shopping_cart.order_list";
    const ORDER_EDIT_VIEW = "comsa_sulu_shopping_cart.order_edit";
    const ORDER_FORM_KEY = "order_details";

    const SETTING_LIST_KEY = "settings";
    const SETTING_LIST_VIEW = "comsa_sulu_shopping_cart.setting_list";
    const SETTING_EDIT_VIEW = "comsa_sulu_shopping_cart.setting_edit";
    const SETTING_FORM_KEY = "setting_details";

    private ViewBuilderFactoryInterface $viewBuilderFactory;

    public function __construct(
        ViewBuilderFactoryInterface $viewBuilderFactory
    ) {
        $this->viewBuilderFactory = $viewBuilderFactory;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void {
        $module = new NavigationItem("comsa_sulu_shopping_cart.webshop_module");
        $module->setPosition(40);
        $module->setIcon("su-dollar");

        $ordersItem =  new NavigationItem("comsa_sulu_shopping_cart.orders");
        $ordersItem->setView(static::ORDER_LIST_VIEW);
        $ordersItem->setPosition(3);
        $module->addChild($ordersItem);

        $settingsItem = new NavigationItem("comsa_sulu_shopping_cart.settings");
        $settingsItem->setPosition(4);
        $settingsItem->setView(static::SETTING_LIST_VIEW);
        $module->addChild($settingsItem);

        $navigationItemCollection->add($module);
    }

    public function configureViews(ViewCollection $viewCollection): void {
        $orderListView = $this->viewBuilderFactory->createListViewBuilder(static::ORDER_LIST_VIEW, "/orders")
            ->setResourceKey(Order::RESOURCE_KEY)
            ->setListKey(static::ORDER_LIST_KEY)
            ->setTitle("comsa_sulu_shopping_cart.orders")
            ->addListAdapters(["table"])
            ->setEditView(static::ORDER_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.delete")
            ])
        ;
        $viewCollection->add($orderListView);

        $orderEditView = $this->viewBuilderFactory->createViewBuilder(static::ORDER_EDIT_VIEW, "/orders/:id", "sulu_shopping_cart.order");
        $viewCollection->add($orderEditView);

        $settingListView = $this->viewBuilderFactory->createListViewBuilder(static::SETTING_LIST_VIEW, "/settings")
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setListKey(static::SETTING_LIST_KEY)
            ->setTitle("comsa_sulu_shopping_cart.settings")
            ->addListAdapters(["table"])
            ->setEditView(static::SETTING_EDIT_VIEW);
        $viewCollection->add($settingListView);

        $settingResourceTab = $this->viewBuilderFactory->createResourceTabViewBuilder(static::SETTING_EDIT_VIEW, "/settings/:id")
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setBackView(static::SETTING_LIST_VIEW)
            ->setTitleProperty("title");
        $viewCollection->add($settingResourceTab);

        $settingEditView = $this->viewBuilderFactory->createFormViewBuilder(static::SETTING_EDIT_VIEW . ".details", "/details")
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setFormKey(static::SETTING_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(static::SETTING_EDIT_VIEW);
        $viewCollection->add($settingEditView);
    }
}
