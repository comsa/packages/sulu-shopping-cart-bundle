<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Admin;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

/**
 * Configures the Sulu Admin to include Products
 * @package Comsa\SuluShoppingCart\Admin
 */
class ProductAdmin extends Admin {

    const PRODUCT_LIST_KEY = "products";
    const PRODUCT_LIST_VIEW = "comsa_sulu_shopping_cart.product_list";
    const PRODUCT_EDIT_VIEW = "comsa_sulu_shopping_cart.product_edit";
    const PRODUCT_FORM_KEY = "product_details";
    const PRODUCT_ADD_VIEW = "comsa_sulu_shopping_cart.product_add";

    const TAX_CATEGORY_LIST_KEY = "tax_categories";
    const TAX_CATEGORY_LIST_VIEW = "comsa_sulu_shopping_cart.tax_category_list";
    const TAX_CATEGORY_EDIT_VIEW = "comsa_sulu_shopping_cart.tax_category_edit";
    const TAX_CATEGORY_FORM_KEY = "tax_category_details";
    const TAX_CATEGORY_ADD_VIEW = "comsa_sulu_shopping_cart.tax_category_add";

    const OPTION_LIST_KEY = "options";
    const OPTION_LIST_VIEW = "comsa_sulu_shopping_cart.option_list";
    const OPTION_EDIT_VIEW = "comsa_sulu_shopping_cart.option_edit";
    const OPTION_FORM_KEY = 'option_details';
    const OPTION_ADD_VIEW = "comsa_sulu_shopping_cart.option_add";

    private ViewBuilderFactoryInterface $viewBuilderFactory;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory) {
        $this->viewBuilderFactory = $viewBuilderFactory;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void {
        $module = $navigationItemCollection->get("comsa_sulu_shopping_cart.webshop_module");
        $module->setIcon('su-storage');

        $productsItem = new NavigationItem('comsa_sulu_shopping_cart.products');
        $productsItem->setPosition(1);
        $productsItem->setView(static::PRODUCT_LIST_VIEW);

        $taxCategoriesItem = new NavigationItem("comsa_sulu_shopping_cart.tax_categories");
        $taxCategoriesItem->setPosition(2);
        $taxCategoriesItem->setView(self::TAX_CATEGORY_LIST_VIEW);

        // $optionsItem = new NavigationItem('comsa_sulu_shopping_cart.options');
        // $optionsItem->setPosition(2);
        // $optionsItem->setView(static::OPTION_LIST_VIEW);

        $module->addChild($productsItem);
        $module->addChild($taxCategoriesItem);
        // $module->addChild($optionsItem);

        $navigationItemCollection->add($module);
    }

    private function configureTaxCategoryViews(ViewCollection $viewCollection): void {
        // List view
        $taxListView = $this->viewBuilderFactory->createListViewBuilder(
          self::TAX_CATEGORY_LIST_VIEW,
          "/tax-categories"
        )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setListKey(self::TAX_CATEGORY_LIST_KEY)
            ->setTitle("comsa_sulu_shopping_cart.tax_categories")
            ->setAddView(self::TAX_CATEGORY_ADD_VIEW)
            ->setEditView(self::TAX_CATEGORY_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.add"),
                new ToolbarAction("sulu_admin.delete")
            ])
            ->addListAdapters(["table"])
        ;
        $viewCollection->add($taxListView);

        // Details resource tab
        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(
                self::TAX_CATEGORY_ADD_VIEW,
                "/tax-categories/add"
            )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setBackView(self::TAX_CATEGORY_LIST_VIEW)
        );

        // Add view
        $addView = $this->viewBuilderFactory->createFormViewBuilder(
            self::TAX_CATEGORY_ADD_VIEW . ".details",
            "/details"
        )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setFormKey(self::TAX_CATEGORY_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->setEditView(self::TAX_CATEGORY_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(self::TAX_CATEGORY_ADD_VIEW)
        ;
        $viewCollection->add($addView);

        // Edit resource tab
        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(
                self::TAX_CATEGORY_EDIT_VIEW,
                "/tax-categories/:id"
            )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setBackView(self::TAX_CATEGORY_LIST_VIEW)
            ->setTitleProperty("title")
        );

        // Edit view
        $editView = $this->viewBuilderFactory->createFormViewBuilder(
            self::TAX_CATEGORY_EDIT_VIEW . ".details",
            "/details"
        )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setFormKey(self::TAX_CATEGORY_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save"),
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setParent(self::TAX_CATEGORY_EDIT_VIEW)
        ;

        $viewCollection->add($editView);
    }

    private function configureProductViews(ViewCollection $viewCollection) {
        $productListView = $this->viewBuilderFactory->createListViewBuilder(
            static::PRODUCT_LIST_VIEW, '/products')
            ->setResourceKey(Product::RESOURCE_KEY)
            ->setListKey(static::PRODUCT_LIST_KEY)
            ->setTitle('comsa_sulu_shopping_cart.products')
            ->setAddView(static::PRODUCT_ADD_VIEW)
            ->setEditView(static::PRODUCT_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.add'),
                new ToolbarAction('sulu_admin.delete'),
            ])
            ->addListAdapters(["table"]);

        $viewCollection->add($productListView);

        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(static::PRODUCT_ADD_VIEW, "/products/add")
            ->setResourceKey(Product::RESOURCE_KEY)
            ->setBackView(static::PRODUCT_LIST_VIEW)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::PRODUCT_ADD_VIEW . '.details', '/details')
            ->setResourceKey(Product::RESOURCE_KEY)
            ->setFormKey(static::PRODUCT_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->setEditView(static::PRODUCT_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.save'),
            ])
            ->setParent(static::PRODUCT_ADD_VIEW)
        );

        $viewCollection->add($this->viewBuilderFactory->createResourceTabViewBuilder(static::PRODUCT_EDIT_VIEW, "/products/:id")
            ->setResourceKey(Product::RESOURCE_KEY)
            ->setBackView(static::PRODUCT_LIST_VIEW)
            ->setTitleProperty('title')
       );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::PRODUCT_EDIT_VIEW . '.details', '/details')
            ->setResourceKey(Product::RESOURCE_KEY)
            ->setFormKey(static::PRODUCT_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.save'),
                new ToolbarAction('sulu_admin.delete')
            ])
            ->setParent(static::PRODUCT_EDIT_VIEW)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createListViewBuilder(
                self::PRODUCT_EDIT_VIEW . '.orders', '/orders'
            )
            ->setResourceKey(Order::RESOURCE_KEY)
            ->setListKey(OrderAdmin::ORDER_LIST_KEY)
            ->setTabTitle("comsa_sulu_shopping_cart.orders")
            ->addListAdapters(["table"])
            ->addRouterAttributesToListRequest(["id" => "id"])
            ->addResourceStorePropertiesToListRequest(["id" => "id"])
            ->addToolbarActions([
                new ToolbarAction('comsa_sulu_shopping_cart.export_orders')
            ])
            ->setParent(self::PRODUCT_EDIT_VIEW)
        );
    }

    private function configureOptionViews(ViewCollection $viewCollection) {
        $optionListView = $this->viewBuilderFactory->createListViewBuilder(
            static::OPTION_LIST_VIEW, '/options')
            ->setResourceKey(Option::RESOURCE_KEY)
            ->setListKey(static::OPTION_LIST_KEY)
            ->setTitle('comsa_sulu_shopping_cart.options')
            ->setAddView(static::OPTION_ADD_VIEW)
            ->setEditView(static::OPTION_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.add'),
                new ToolbarAction('sulu_admin.delete'),
            ])
            ->addListAdapters(["table"]);

        $viewCollection->add($optionListView);

        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(static::OPTION_ADD_VIEW, "/options/add")
                ->setResourceKey(Option::RESOURCE_KEY)
                ->setBackView(static::OPTION_LIST_VIEW)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::OPTION_ADD_VIEW . '.details', '/details')
                ->setResourceKey(Option::RESOURCE_KEY)
                ->setFormKey(static::OPTION_FORM_KEY)
                ->setTabTitle('sulu_admin.details')
                ->setEditView(static::OPTION_EDIT_VIEW)
                ->addToolbarActions([
                    new ToolbarAction('sulu_admin.save'),
                ])
                ->setParent(static::OPTION_ADD_VIEW)
        );

        $viewCollection->add($this->viewBuilderFactory->createResourceTabViewBuilder(static::OPTION_EDIT_VIEW, "/options/:id")
            ->setResourceKey(Option::RESOURCE_KEY)
            ->setBackView(static::OPTION_LIST_VIEW)
            ->setTitleProperty('title')
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::OPTION_EDIT_VIEW . '.details', '/details')
                ->setResourceKey(Option::RESOURCE_KEY)
                ->setFormKey(static::OPTION_FORM_KEY)
                ->setTabTitle('sulu_admin.details')
                ->addToolbarActions([
                    new ToolbarAction('sulu_admin.save')
                ])
                ->setParent(static::OPTION_EDIT_VIEW)
        );
    }

    public function configureViews(ViewCollection $viewCollection): void {
        $this->configureProductViews($viewCollection);
        $this->configureTaxCategoryViews($viewCollection);
        $this->configureOptionViews($viewCollection);
    }
}
