<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Event\Product;

use Comsa\SuluShoppingCart\Entity\Product;
use Symfony\Contracts\EventDispatcher\Event;

class ProductUpdatedEvent extends Event {
    const NAME = "comsa.sc.product.updated";

    private Product $product;
    private string $locale;
    private string $webspaceKey;

    public function __construct(Product $product, string $locale, string $webspaceKey) {
        $this->product = $product;
        $this->locale = $locale;
        $this->webspaceKey = $webspaceKey;
    }

    public function getProduct(): Product {
        return $this->product;
    }

    public function getLocale(): string {
        return $this->locale;
    }

    public function getWebspaceKey(): string {
        return $this->webspaceKey;
    }
}
