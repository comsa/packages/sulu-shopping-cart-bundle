<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Event\Product;

use Comsa\SuluShoppingCart\Entity\Product;
use Symfony\Contracts\EventDispatcher\Event;

class ProductDeletedEvent extends Event {
    const NAME = "comsa.sc.product.deleted";

    private Product $product;

    public function __construct(Product $product) {
        $this->product = $product;
    }

    public function getProduct(): Product {
        return $this->product;
    }
}
