<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Event;

use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\Customer;
use Comsa\SuluShoppingCart\Entity\Order;
use Symfony\Contracts\EventDispatcher\Event;

class OrderFulfilledEvent extends Event {

    private Order $order;

    public function __construct(Order $order) {
        $this->order = $order;
    }

    public function getOrder(): Order {
        return $this->order;
    }

    public function getCart() : Cart {
        return $this->order->getCart();
    }
}
