<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Event;

use Cassandra\Custom;
use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\Customer;
use Comsa\SuluShoppingCart\Entity\Order;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event That gets triggered when order got confirmed
 * @package Comsa\SuluShoppingCart\Event
 */
class OrderConfirmedEvent extends Event {

    CONST NAME = 'comsa.sc.order.confirmed';

    private Order $order;

    public function __construct(Order $order) {
        $this->order = $order;
    }

    public function getOrder(): Order {
        return $this->order;
    }

    public function getCart(): Cart {
        return $this->order->getCart();
    }

    public function getCustomer(): Customer {
        $cart = $this->getCart();
        return $cart->getCustomer();
    }
}
