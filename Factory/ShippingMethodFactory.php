<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\ShippingMethod;

/**
 * Handles Creation of Shipping Method
 */
class ShippingMethodFactory {
    public static function create(
        string $name,
        string $type,
        float $price
    ): ShippingMethod {
        return (new ShippingMethod())
            ->setName($name)
            ->setType($type)
            ->setPrice($price)
        ;
    }
}
