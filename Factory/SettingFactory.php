<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\Setting;

/**
 * Handles creation of Setting
 */
class SettingFactory {
    public static function create(string $title, string $key, ?string $value): Setting {
        return (new Setting())
            ->setTitle($title)
            ->setKey($key)
            ->setValue($value)
        ;
    }
}
