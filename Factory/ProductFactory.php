<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Doctrine\Common\Collections\Collection;
use Sulu\Bundle\MediaBundle\Entity\Media;
use Sulu\Bundle\MediaBundle\Entity\MediaInterface;
use Sulu\Bundle\PageBundle\Document\PageDocument;

/**
 * Handles Creation of Products
 * @package Comsa\SuluShoppingCart\Factory
 */
class ProductFactory {
    public static function create(
        ?string $code,
        ?string $title,
        ?string $description,
        ?float $price,
        bool $followStock,
        ?int $stock,
        string $unit,
        string $paymentMethodType,
        ?string $subscriptionInterval,
        ?int $subscriptionAmount,
        ?bool $timeLimitedAvailability,
        ?\DateTime $startTimeAvailability,
        ?\DateTime $endTimeAvailability,
        ?string $optionSelectionType,
        ?Media $thumbnail,
        string $parent,
        Collection $options,
        ?TaxCategory $taxCategory
    ): Product {
        return (new Product())
            ->setCode($code)
            ->setTitle($title)
            ->setDescription($description)
            ->setPrice($price)
            ->setFollowStock($followStock)
            ->setStock($stock)
            ->setUnit($unit)
            ->setPaymentMethodType($paymentMethodType)
            ->setSubscriptionInterval($subscriptionInterval)
            ->setSubscriptionAmount($subscriptionAmount)
            ->setTimeLimitedAvailbility($timeLimitedAvailability)
            ->setStartTimeAvailability($startTimeAvailability)
            ->setEndTimeAvailability($endTimeAvailability)
            ->setOptionSelectionType($optionSelectionType)
            ->setThumbnail($thumbnail)
            ->setParentPage($parent)
            ->setOptions($options)
            ->setTaxCategory($taxCategory);
        ;
    }
}
