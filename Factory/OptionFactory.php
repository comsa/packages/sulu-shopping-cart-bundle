<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Product;

/**
 * Handles creation for Product Options
 * @package Comsa\SuluShoppingCart\Factory
 */
class OptionFactory
{
    public static function create(
        string $title,
        float $price,
    ): Option {
        return (new Option())
            ->setTitle($title)
            ->setPrice($price)
        ;
    }
}
