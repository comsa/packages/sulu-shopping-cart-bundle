<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\PaymentMethod;

/**
 * Handles the creation of Payment Method
 * @package Comsa\SuluShoppingCart\Factory
 */
class PaymentMethodFactory
{
    public static function create(
        string $name,
        string $type,
        float $price
    ): ?PaymentMethod {
        return (new PaymentMethod())
            ->setName($name)
            ->setType($type)
            ->setPrice($price)
        ;
    }
}
