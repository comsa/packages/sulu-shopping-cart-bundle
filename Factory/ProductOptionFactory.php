<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\ProductOption;

/**
 * Handles the creation of ProductOptions
 * @package Comsa\SuluShoppingCart\Factory
 */
class ProductOptionFactory {
    public static function create(
        ?Product $product,
        Option $option,
        int $position
    ): ProductOption {
        return (new ProductOption())
            ->setProduct($product)
            ->setOption($option)
            ->setPosition($position)
        ;
    }
}
