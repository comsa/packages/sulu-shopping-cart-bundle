<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Factory;

use Comsa\SuluShoppingCart\Entity\Address;

/**
 * Handles Creation of Address
 * @package Comsa\SuluShoppingCart\Factory
 */
class AddressFactory {
    public static function create(
        string $street,
        string $streetNumber,
        string $postal,
        string $city,
        string $country
    ): Address {
        return (new Address())
            ->setStreet($street)
            ->setStreetNumber($streetNumber)
            ->setPostal($postal)
            ->setCity($city)
            ->setCountry($country)
        ;
    }
}
