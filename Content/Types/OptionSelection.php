<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Content\Types;


use Comsa\SuluShoppingCart\Entity\Option;
use Doctrine\ORM\EntityManagerInterface;
use PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\ComplexContentType;

/**
 * Content Type for Product Options
 * @package Comsa\SuluShoppingCart\Content\Types
 */
class OptionSelection extends ComplexContentType {

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey) {
        $optionId = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($personId);
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentKey) {
        $optionIds = [];
        $value = $property->getValue();

        if (null === $value) {
            $node->setProperty($property->getName(), null);
            return;
        }

        foreach ($value as $option) {
            if (is_numeric($option)) {
                $optionIds[] = $option;
            } else {
                $optionIds[] = $option['id'];
            }
        }

        $node->setProperty($property->getName(), $optionIds);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey) {
        if ($node->hasProperty($property->getName())) {
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property) {
        $optionIds = $property->getValue();
        foreach ($optionIds as $option) {
            $option = $this->entityManager->getRepository(Option::class)->find($option);
        }

        return $optionIds;
    }
}
