<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Content\Types;

use Comsa\SuluShoppingCart\Repository\TaxCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;

/**
 * Content Type for a Single Tax Category
 * @package Comsa\SuluShoppingCart\Content\Types
 */
class SingleTaxCategorySelection extends SimpleContentType {
    private TaxCategoryRepository $taxCategoryRepository;

    public function __construct(TaxCategoryRepository $taxCategoryRepository) {
        $this->taxCategoryRepository = $taxCategoryRepository;
        parent::__construct("comsa.sc.single_selection.tax_category", null);
    }

    public function getContentData(PropertyInterface $property) {
        $taxCategoryId = $property->getValue();

        if (!$taxCategoryId) {
            return null;
        }

        $taxCategory = $this->taxCategoryRepository->find($taxCategoryId);

        return $taxCategory;
    }
}
