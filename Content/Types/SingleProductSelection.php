<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Content\Types;


use Comsa\SuluShoppingCart\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;

/**
 * Content Type for a Single Product
 * @package Comsa\SuluShoppingCart\Content\Types
 */
class SingleProductSelection extends SimpleContentType {
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
        parent::__construct("single_product_selection", null);
    }

    public function getContentData(PropertyInterface $property) {
        $productId = $property->getValue();
        if (!$productId) {
            return null;
        }

        $product = $this->entityManager->getRepository(Product::class)->find($productId);

        return $product;
    }
}
