<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Content\Types;

use Sulu\Component\Content\SimpleContentType;

/**
 * Content Type for Money
 * @package Comsa\SuluShoppingCart\Content\Types
 */
class Money extends SimpleContentType {
}
