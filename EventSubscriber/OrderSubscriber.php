<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\EventSubscriber;

use App\Entity\CartItem;
use Comsa\SuluShoppingCart\Entity\Customer;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Event\OrderConfirmedEvent;
use Comsa\SuluShoppingCart\Event\OrderFulfilledEvent;
use Comsa\SuluShoppingCart\Factory\EmailFactory;
use Comsa\SuluShoppingCart\Repository\PaymentRepository;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Subscribed to the Order Confirmed Event and sends the confirmation mail
 * @package Comsa\SuluShoppingCart\EventListener
 */
class OrderSubscriber implements EventSubscriberInterface {
    private MailerInterface $mailer;
    private ParameterBagInterface $parameterBag;
    private TranslatorInterface $translator;
    private Environment $twig;
    private EntityManagerInterface $em;
    private SettingRepository $settingRepository;
    private PaymentRepository $paymentRepository;

    public function __construct(
        MailerInterface $mailer,
        ParameterBagInterface $parameterBag,
        TranslatorInterface $translator,
        Environment $twig,
        EntityManagerInterface $em,
        SettingRepository $settingRepository,
        PaymentRepository $paymentRepository
    ) {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->em = $em;
        $this->settingRepository = $settingRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public static function getSubscribedEvents(): array {
        return [
            OrderConfirmedEvent::NAME => "onOrderConfirmed",
        ];
    }

    public function onOrderConfirmed(OrderConfirmedEvent $event): void {
        $order = $event->getOrder();
        $cart = $event->getCart();
        $customer = $event->getCustomer();
        $note = $order->getNote();
        $cartItems = $cart->getItems();
        $payment = $this->paymentRepository->findOneByOrder($order);
        $total = $payment->getAmount();

        //-- Loop cartitems to remove quantities
        /** @var CartItem $cartItem */
        foreach ($cartItems as $cartItem) {
            $product = $cartItem->getProduct();
            $quantity = $cartItem->getQuantity();

            if ($product->getFollowStock()) {
                $product->setStock($product->getStock() - $quantity);
            }
        }

        $this->em->flush();

        $baseCartItemProperties = ["id", "product", "uuid", "quantity", "title", "options", "price", "discr"];
        $customCartItemProperties = [];
        $metadata =$this->em->getClassMetadata(CartItem::class)->getFieldNames();
        foreach ($metadata as $property) {
            if (!in_array($property, $baseCartItemProperties)) {
                $customCartItemProperties[] = $property;
            }
        }

        $baseOrderProperties = ["id", "cart", "status", "note", "payment", "createdAt", "updatedAt", "discr", "total"];
        $customOrderProperties = [];
        $metadata = $this->em->getClassMetadata(\App\Entity\Order::class)->getFieldNames();
        foreach ($metadata as $property) {
            if (!in_array($property, $baseOrderProperties)) {
                $customOrderProperties[] = $property;
            }
        }

        $this->sendStandardEmail($order, $cartItems->toArray(), $customer, $customCartItemProperties, $customOrderProperties, $total);
    }

    private function sendStandardEmail(Order $order, array $cartItems, Customer $customer, array $customCartItemProperties, array $customOrderProperties, $total): void {
        $from = $this->settingRepository->findOneByKey(SettingEnum::KEYS_EMAIL)->getValue();
        //-- Send standard e-mail
        $email = EmailFactory::create(
            from: $from,
            to: [$customer->getEmail()],
            cc: [$from],
            bcc: "notifications@comsa.be" ,
            subject: $this->translator->trans("comsa_sulu_shopping_cart.confirmation_subject"),
            template: "@SuluShoppingCart/mail/build/order-confirmation.html.twig",
            params: [
                "order" => $order,
                "cartItems" => $cartItems,
                "customer" => $customer,
                "customCartItemProperties" => $customCartItemProperties,
                "customOrderProperties" => $customOrderProperties,
                "total" => $total
            ]
        );

        $this->mailer->send($email);
    }
}

