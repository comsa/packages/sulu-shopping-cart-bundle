<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\EventSubscriber;

use Comsa\SuluShoppingCart\Event\Product\ProductCreatedEvent;
use Comsa\SuluShoppingCart\Event\Product\ProductDeletedEvent;
use Comsa\SuluShoppingCart\Event\Product\ProductUpdatedEvent;
use Comsa\SuluShoppingCart\Service\DocumentService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductSubscriber implements EventSubscriberInterface {
    private DocumentService $documentService;

    public function __construct(DocumentService $documentService) {
        $this->documentService = $documentService;
    }

    public static function getSubscribedEvents() {
        return [
            ProductCreatedEvent::NAME => "onProductCreated",
            ProductUpdatedEvent::NAME => "onProductUpdated",
            ProductDeletedEvent::NAME => "onProductDeleted"
        ];
    }

    public function onProductCreated(ProductCreatedEvent $event): void {
        $this->documentService->createProductPage(
            $event->getProduct(),
            $event->getLocale(),
            $event->getWebspaceKey()
        );
    }

    public function onProductUpdated(ProductUpdatedEvent $event): void {
        $this->documentService->createProductPage(
            $event->getProduct(),
            $event->getLocale(),
            $event->getWebspaceKey()
        );
    }

    public function onProductDeleted(ProductDeletedEvent $event): void {
        $this->documentService->deleteProductPage($event->getProduct());
    }
}
