<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Product::class);
    }

    public function findOneByTitle(string $title): ?Product {
        return $this->createQueryBuilder("product")
            ->where("product.title = :title")
            ->setParameter("title", $title)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findLatest(): ?Product {
        return $this->createQueryBuilder("p")
            ->orderBy("p.id", "DESC")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
