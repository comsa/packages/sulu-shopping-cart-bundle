<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AddressRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Address::class);
    }

    public function findAddress(
        string $street,
        string $streetNumber,
        string $postal,
        string $city,
        string $country
    ): ?Address {
        return $this->createQueryBuilder("address")
            ->andWhere("address.street = :street")
            ->andWhere("address.streetNumber = :streetNumber")
            ->andWhere("address.postal = :postal")
            ->andWhere("address.city = :city")
            ->andWhere("address.country = :country")
            ->setParameter("street", $street)
            ->setParameter("streetNumber", $streetNumber)
            ->setParameter("postal", $postal)
            ->setParameter("city", $city)
            ->setParameter("country", $country)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
