<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\CartItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CartItemRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, CartItem::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?CartItem {
        return parent::find($id, $lockMode, $lockVersion);
    }
}
