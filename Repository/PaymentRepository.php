<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PaymentRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Payment::class);
    }

    public function findOneByExternalId(string $externalId): ?Payment {
        return $this->createQueryBuilder("payment")
            ->where("payment.externalId = :externalId")
            ->setParameter("externalId", $externalId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneByExternalSubscriptionId(string $externalSubscriptionId): ?Payment {
        return $this->createQueryBuilder("payment")
            ->where("payment.externalSubscriptionId = :externalSubscriptionId")
            ->setParameter("payment.externalSubscriptionId", $externalSubscriptionId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneByOrder(Order $order): ?Payment {
        return $this->createQueryBuilder("payment")
            ->where("payment.order = :order")
            ->setParameter("order", $order)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
