<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\Setting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SettingRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Setting::class);
    }

    public function findOneByKey(string $key): ?Setting {
        return $this->createQueryBuilder("setting")
            ->where("setting.key = :key")
            ->setParameter("key", $key)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
