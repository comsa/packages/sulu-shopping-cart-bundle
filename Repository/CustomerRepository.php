<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\Address;
use Comsa\SuluShoppingCart\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CustomerRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Customer::class);
    }

    public function findDuplicate(string $firstname, string $lastname, string $email, string $phone, ?Address $billingAddress) {
        return $this->createQueryBuilder("c")
            ->andWhere("c.firstName = :firstname")
            ->andWhere("c.lastName = :lastname")
            ->andWhere("c.email = :email")
            ->andWhere("c.phoneNumber = :phone")
            ->andWhere("c.billingAddress = :billingAddress")
            ->setParameter("firstname", $firstname)
            ->setParameter("lastname", $lastname)
            ->setParameter("email", $email)
            ->setParameter("phone", $phone)
            ->setParameter("billingAddress", $billingAddress)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
