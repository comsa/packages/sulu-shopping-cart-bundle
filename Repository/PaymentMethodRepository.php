<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Repository;

use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PaymentMethodRepository extends ServiceEntityRepository {
    
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, PaymentMethod::class);
    }

    public function findOneByType(string $type) : PaymentMethod {
        return $this->createQueryBuilder("paymentMethod")
            ->where("paymentMethod.type = :type")
            ->setParameter("type", $type)
            ->getQuery()
            ->getSingleResult()
        ;
    }
}
