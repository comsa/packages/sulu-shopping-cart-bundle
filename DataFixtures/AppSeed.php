<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\DataFixtures;

use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Factory\SettingFactory;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Comsa\SuluShoppingCart\Entity\Setting;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Collection\Set;

/**
 * Fixtures for Shopping cart settings
 * @package Comsa\SuluShoppingCart\DataFixtures
 */
class AppSeed extends Fixture {

    private SettingRepository $settingRepository;

    public function __construct(SettingRepository $settingRepository) {
        $this->settingRepository = $settingRepository;
    }

    public function load(ObjectManager $manager) {
        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_PAYMENT_METHOD)) {
            $manager->persist($this->loadSettingPaymentMethod());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_SHIPPING_METHOD)) {
            $manager->persist($this->loadSettingShippingMethod());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_IBAN)) {
            $manager->persist($this->loadSettingIBAN());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_BIC)) {
            $manager->persist($this->loadSettingBIC());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_EMAIL)) {
            $manager->persist($this->loadSettingEmail());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_ORDER_COMPLETED_TEXT)) {
            $manager->persist($this->loadSettingOrderCompletedText());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_FREE_ORDER_DATE)) {
            $manager->persist($this->loadSettingFreeOrderDate());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_FREE_ORDER_DATE_DAYS_BEFORE)) {
            $manager->persist($this->loadSettingMinDayBefore());
        }

        $manager->flush();
    }

    public function loadSettingPaymentMethod(): Setting {
        return SettingFactory::create(
            title: SettingEnum::TITLES_PAYMENT_METHOD,
            key: SettingEnum::KEYS_PAYMENT_METHOD,
            value:null
        );
    }

    public function loadSettingShippingMethod(): Setting {
        return SettingFactory::create(
          title: SettingEnum::TITLES_SHIPPING_METHOD,
          key: SettingEnum::KEYS_SHIPPING_METHOD,
          value: null
        );
    }

    public function loadSettingIBAN(): Setting {
        return SettingFactory::create(
          title: SettingEnum::TITLES_IBAN,
          key: SettingEnum::KEYS_IBAN,
          value: SettingEnum::KEYS_IBAN
        );
    }

    public function loadSettingBIC(): Setting {
        return SettingFactory::create(
            title: SettingEnum::TITLES_BIC,
            key: SettingEnum::KEYS_BIC,
            value: SettingEnum::KEYS_BIC
        );
    }

    public function loadSettingEmail(): Setting {
        return SettingFactory::create(
          title: SettingEnum::TITLES_EMAIL,
          key: SettingEnum::KEYS_EMAIL,
          value: SettingEnum::KEYS_EMAIL
        );
    }

    public function loadSettingOrderCompletedText(): Setting {
        return SettingFactory::create(
            title: SettingEnum::TITLES_ORDER_COMPLETED_TEXT,
            key: SettingEnum::KEYS_ORDER_COMPLETED_TEXT,
            value: SettingEnum::TITLES_ORDER_COMPLETED_TEXT
        );
    }

    public function loadSettingFreeOrderDate(): Setting {
        return SettingFactory::create(
          title: SettingEnum::TITLES_FREE_ORDER_DATE,
          key: SettingEnum::KEYS_FREE_ORDER_DATE,
          value: "false"
        );
    }

    public function loadSettingMinDayBefore(): Setting {
        return SettingFactory::create(
            title: SettingEnum::TITLES_FREE_ORDER_DATE_DAYS_BEFORE,
            key: SettingEnum::KEYS_FREE_ORDER_DATE_DAYS_BEFORE,
            value: "0"
        );
    }
}
