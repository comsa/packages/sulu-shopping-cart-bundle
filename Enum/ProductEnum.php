<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Enum;

class ProductEnum {
    const UNIT_KG = "kg";
    const UNIT_EACH = "each";
    const UNIT_POT = "pot";
    const UNIT_PERSON = "person";
    const UNIT_JAR = "jar";
    const UNIT_LITER = "liter";
    const UNIT_TEN_KG = "ten_kg";
    const UNIT_ARRAY = [
        self::UNIT_KG,
        self::UNIT_EACH,
        self::UNIT_POT,
        self::UNIT_PERSON,
        self::UNIT_JAR,
        self::UNIT_LITER,
        self::UNIT_TEN_KG
    ];

    const SUBSCRIPTION_INTERVAL_WEEKLY = "WEEKLY";
    const SUBSCRIPTION_INTERVAL_MONTHLY = "MONTHLY";
    const SUBSCRIPTION_INTERVAL_YEARLY = "YEARLY";
    const SUBSCRIPTION_INTERVAL_ARRAY = [
        self::SUBSCRIPTION_INTERVAL_WEEKLY,
        self::SUBSCRIPTION_INTERVAL_MONTHLY,
        self::SUBSCRIPTION_INTERVAL_YEARLY
    ];
}
