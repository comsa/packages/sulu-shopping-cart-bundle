<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Enum;

/**
 * Defines the constants for Settings
 * @package Comsa\SuluShoppingCart\Enum
 */
class SettingEnum {
    const TITLES_PAYMENT_METHOD = "Payment Method";
    const TITLES_SHIPPING_METHOD = "Shipping Method";
    const TITLES_IBAN = "IBAN";
    const TITLES_BIC = "BIC";
    const TITLES_EMAIL = "Email";
    const TITLES_ORDER_COMPLETED_TEXT = "Text after order";
    const TITLES_FREE_ORDER_DATE = "Free delivery date input";
    const TITLES_FREE_ORDER_DATE_DAYS_BEFORE = "Minimum days before delivery date";
    const TITLES_ARRAY = [
        self::TITLES_PAYMENT_METHOD,
        self::TITLES_IBAN,
        self::TITLES_BIC,
        self::TITLES_ORDER_COMPLETED_TEXT,
        self::TITLES_FREE_ORDER_DATE,
        self::TITLES_FREE_ORDER_DATE_DAYS_BEFORE
    ];

    const KEYS_PAYMENT_METHOD = "paymentMethod";
    const KEYS_SHIPPING_METHOD = "shippingMethod";
    const KEYS_IBAN = "IBAN";
    const KEYS_BIC = "BIC";
    const KEYS_EMAIL = "email";
    const KEYS_ORDER_COMPLETED_TEXT = "orderCompletedText";
    const KEYS_FREE_ORDER_DATE = "freeDeliveryDateInput";
    const KEYS_FREE_ORDER_DATE_DAYS_BEFORE = "minDaysBefore";
    const KEYS_ARRAY = [
        self::KEYS_PAYMENT_METHOD,
        self::KEYS_IBAN,
        self::KEYS_BIC,
        self::KEYS_ORDER_COMPLETED_TEXT,
        self::KEYS_FREE_ORDER_DATE,
        self::KEYS_FREE_ORDER_DATE_DAYS_BEFORE
    ];

    const VALUES_PAYMENT_METHOD_MOLLIE = "mollie";
    const VALUES_PAYMENT_METHOD_BANK = "bank";
    const VALUES_PAYMENT_METHOD_CASH = "cash";
    const VALUES_PAYMENT_METHOD_ARRAY = [
        self::VALUES_PAYMENT_METHOD_MOLLIE,
        self::VALUES_PAYMENT_METHOD_BANK,
        self::VALUES_PAYMENT_METHOD_CASH
    ];

    const VALUES_SHIPPING_METHOD_DELIVER = "deliver";
    const VALUES_SHIPPING_METHOD_PICKUP = "pickup";
    const VALUES_SHIPPING_METHOD_ARRAY = [
        self::VALUES_SHIPPING_METHOD_DELIVER,
        self::VALUES_SHIPPING_METHOD_PICKUP
    ];

    const FORM_KEY_PAYMENT_METHOD = "valuePaymentMethod";
    const FORM_KEY_SHIPPING_METHOD = "valueShippingMethod";
    const FORM_KEY_ORDER_COMPLETED_TEXT = "valueOrderCompletedText";
    const FORM_KEY_EMAIL = "valueEmail";
    const FORM_KEY_DEFAULT = "value";
    const FORM_KEY_FREE_ORDER_DATE = "freeDateInput";
    const FORM_KEY_FREE_ORDER_DAYS_BEFORE = "minDaysBefore";
    const FORM_KEY_NAME_DELIVER = "nameDeliver";
    const FORM_KEY_NAME_PICKUP = "namePickup";
    const FORM_KEY_PRICE_DELIVER = "priceDeliver";
    const FORM_KEY_PRICE_PICKUP = "pricePickup";
    const FORM_KEY_NAME_MOLLIE = "nameMollie";
    const FORM_KEY_NAME_BANK = "nameBank";
    const FORM_KEY_NAME_CASH = "nameCash";
    const FORM_KEY_PRICE_MOLLIE = "priceMollie";
    const FORM_KEY_PRICE_BANK = "priceBank";
    const FORM_KEY_PRICE_CASH = "priceCash";
    const FORM_KEY_ARRAY = [
        self::FORM_KEY_PAYMENT_METHOD,
        self::FORM_KEY_ORDER_COMPLETED_TEXT,
        self::FORM_KEY_EMAIL,
        self::FORM_KEY_DEFAULT,
        self::FORM_KEY_NAME_MOLLIE,
        self::FORM_KEY_NAME_BANK,
        self::FORM_KEY_NAME_CASH,
        self::FORM_KEY_PRICE_MOLLIE,
        self::FORM_KEY_PRICE_BANK,
        self::FORM_KEY_PRICE_CASH,
        self::FORM_KEY_FREE_ORDER_DATE,
        self::FORM_KEY_FREE_ORDER_DAYS_BEFORE
    ];
}
