<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Handles the Dependency Injection
 * @package Comsa\SuluShoppingCart\DependencyInjection
 */
class SuluShoppingCartExtension extends Extension implements PrependExtensionInterface {

    public function load(array $configs, ContainerBuilder $container) {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__. "/../Resources/config"));
        $loader->load("services.yaml");

        if ($container->hasExtension("twig")) {
            $container->loadFromExtension("twig", [
                "paths" => [
                    "%kernel.root_dir%/../vendor/comsa/sulu-shopping-cart/Resources/views"
                ]
            ]);
        }
    }

    public function prepend(ContainerBuilder $container) {
        if ($container->hasExtension("fos_js_routing")) {
            $container->prependExtensionConfig(
                "fos_js_routing",
                [
                    "routes_to_expose" => [
                        "comsa_sulu_shopping_cart.get_orders",
                        "comsa_sulu_shopping_cart.get_order",
                        "comsa_sulu_shopping_cart.get_products",
                        "comsa_sulu_shopping_cart.get_product",
                        "comsa_sulu_shopping_cart.get_tax-categories",
                        "comsa_sulu_shopping_cart.get_tax-category",
                        "comsa_sulu_shopping_cart.get_options",
                        "comsa_sulu_shopping_cart.get_option",
                        "comsa_sulu_shopping_cart.get_settings",
                        "comsa_sulu_shopping_cart.get_setting",
                        "comsa_sulu_shopping_cart.put_order_paid",
                        "comsa_sulu_shopping_cart.export_order"
                    ],
                ]
            );
        }

        if ($container->hasExtension("sulu_admin")) {
            $container->prependExtensionConfig(
                "sulu_admin", [
                    "lists" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/lists"
                        ]
                    ],
                    "forms" => [
                      "directories" => [
                          __DIR__ . "/../Resources/config/forms"
                      ]
                    ],
                    "resources" => [
                        "orders" => [
                            "routes" => [
                                "list" => "comsa_sulu_shopping_cart.get_orders",
                                "detail" => "comsa_sulu_shopping_cart.get_order",
                            ]
                        ],
                        "settings" => [
                            "routes" => [
                                "list" => "comsa_sulu_shopping_cart.get_settings",
                                "detail" => "comsa_sulu_shopping_cart.get_setting"
                            ]
                        ],
                        "products" => [
                            "routes" => [
                                "list" => "comsa_sulu_shopping_cart.get_products",
                                "detail" => "comsa_sulu_shopping_cart.get_product"
                            ]
                        ],
                        "tax_categories" => [
                          "routes" => [
                              "list" => "comsa_sulu_shopping_cart.get_tax-categories",
                              "detail" => "comsa_sulu_shopping_cart.get_tax-category"
                          ]
                        ],
                        "options" => [
                            "routes" => [
                                "list" => "comsa_sulu_shopping_cart.get_options",
                                "detail" => "comsa_sulu_shopping_cart.get_option"
                            ]
                        ]
                    ],
                    "field_type_options" => [
                        "selection" => [
                            "product_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "pages",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "column_list",
                                        "list_key" => "pages",
                                        "display_properties" => ["title", "url"],
                                        "icon" => "su-document",
                                        "label" => "sulu_page.selection_label",
                                        "overlay_title" => "sulu_page.selection_overlay_title"
                                    ]
                                ]
                            ],
                            "option_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "options",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => 'options',
                                        "display_properties" => [
                                            "title"
                                        ],
                                        "icon" => "su-list-ul",
                                        "label" => "Options",
                                        "overlay_title" => "Select your options"
                                    ]
                                ]
                            ]
                        ],
                        "single_selection" => [
                            "single_product_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "products",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "products",
                                        "display_properties" => [
                                            "title"
                                        ],
                                        "icon" => "su-list-ul",
                                        "overlay_title" => "Select your product",
                                        "empty_text" => "Select your product"
                                    ]
                                ]
                            ],
                            "comsa.sc.single_selection.tax_category" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "tax_categories",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "tax_categories",
                                        "display_properties" => [
                                            "title",
                                            "percentage"
                                        ],
                                        "icon" => "su-list-ul",
                                        "overlay_title" => "Select Tax Category",
                                        "empty_text" => "Select Tax Category"
                                    ]
                                ]
                            ],
                        ]
                    ]
                ]
            );
        }
    }
}
