<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\Content\Types\ResourceLocator\Strategy\ResourceLocatorStrategyPoolInterface;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;

/**
 * Service that provides basic creation and deletion of product pages.
 * @package Comsa\SuluShoppingCart\Service
 */
class DocumentService
{
    private ProductService $productService;
    private DocumentManagerInterface $documentManager;
    private ResourceLocatorStrategyPoolInterface $resourceLocatorStrategyPool;

    public function __construct(
        ProductService $productService,
        DocumentManagerInterface $documentManager,
        ResourceLocatorStrategyPoolInterface $resourceLocatorStrategyPool
    ) {
        $this->productService = $productService;
        $this->documentManager = $documentManager;
        $this->resourceLocatorStrategyPool = $resourceLocatorStrategyPool;
    }

    public function createProductPage(Product $product, string $locale, string $webspaceKey) {
        // get parent
        /** @var PageDocument $parent */
        $parent = $this->documentManager->find($product->getParentPage());

        // delete existing page
        $this->deleteProductPage($product);

        // creation
        /** @var PageDocument $productPage */
        $productPage = $this->documentManager->create("page");
        $productPage->setTitle($product->getTitle());
        $productPage->setParent($parent);
        $productPage->setLocale($locale);
        $url = $this->resourceLocatorStrategyPool
            ->getStrategyByWebspaceKey($webspaceKey)
            ->generate($product->getTitle(), $parent->getUuid(), $webspaceKey, $locale);

        $productPage->setResourceSegment($url);
        $productPage->setStructureType("comsa_product");

        $productPage->getStructure()->bind([
            "title" => $product->getTitle(),
            "url" => $url,
            "product" => $product->getId()
        ]);

        $this->documentManager->persist($productPage, $locale);
        $this->documentManager->publish($productPage, $locale);
        $this->documentManager->flush();

        $this->productService->updatePage($product, $productPage->getUuid());
    }

    public function deleteProductPage(Product $product): void {
        try {
            $this->documentManager->remove(
                $this->documentManager->find($product->getPageId())
            );
            $this->documentManager->flush();
        } finally {
            return;
        }
    }
}
