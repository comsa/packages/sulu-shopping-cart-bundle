<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Entity\ShippingMethod;
use Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException;
use Comsa\SuluShoppingCart\Repository\PaymentMethodRepository;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Comsa\SuluShoppingCart\Repository\ShippingMethodRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseOrmSearchInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the Setting class.
 * @package Comsa\SuluReservations\Service
 */
class SettingService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, SettingRepository $settingRepository) {
        parent::__construct($entityManager, $settingRepository, Setting::class);
    }

    public function create(array $data): CrudResource {
        // Not needed, no settings can be created manually
    }

    /**
     * @param Setting $entity
     * @param array $data
     * @throws InvalidCrudResourceException
     */
    public function update(CrudResource $entity, array $data): void {
        $entity->setValue($data["value"]);
        $this->save($entity);
    }

}
