<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\ShippingMethod;
use Comsa\SuluShoppingCart\Factory\ShippingMethodFactory;
use Comsa\SuluShoppingCart\Repository\ShippingMethodRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseOrmSearchInterface;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the ShippingMethod class.
 * @package Comsa\SuluShoppingCart\Service
 */
class ShippingMethodService extends AbstractCrudService implements  BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, ShippingMethodRepository $repository) {
        parent::__construct($entityManager, $repository, ShippingMethod::class);
    }

    /**
     * @param ShippingMethod $entity
     * @throws \Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException
     */
    public function delete(CrudResource $entity): void {
        $this->validateEntity($entity);
        $entity->hide();
        $this->save($entity);
    }

    /**
     * @param array $data
     * @return CrudResource
     */
    public function create(array $data): CrudResource {
        $entity = ShippingMethodFactory::create(
            $data["name"],
            $data["type"],
            TypeConverter::stringToFloat($data["price"])
        );

        $this->save($entity);

        return $entity;
    }

    /**
     * @param ShippingMethod $entity
     * @param array $data
     * @throws \Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException
     */
    public function update(CrudResource $entity, array $data): void {
        $entity->setName($data["name"]);
        $entity->setPrice(TypeConverter::stringToFloat($data["price"]));
        $entity->show();
        $this->save($entity);
    }
}
