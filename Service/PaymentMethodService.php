<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Factory\PaymentMethodFactory;
use Comsa\SuluShoppingCart\Repository\PaymentMethodRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD service for the PaymentMethod class.
 * @package Comsa\SuluShoppingCart\Service
 */
class PaymentMethodService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, PaymentMethodRepository $paymentMethodRepository) {
        parent::__construct($entityManager, $paymentMethodRepository, PaymentMethod::class);
    }

    /**
     * @param PaymentMethod $entity
     * @throws \Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException
     */
    public function delete(CrudResource $entity): void {
        $this->validateEntity($entity);
        $entity->hide();
        $this->save($entity);
    }

    /**
     * @param array $data
     * @return CrudResource
     */
    public function create(array $data): CrudResource {
        $entity = PaymentMethodFactory::create(
          $data["name"],
          $data["type"],
          $data["price"]
        );

        $this->save($entity);
        return $entity;
    }

    /**
     * @param PaymentMethod $entity
     * @param array $data
     * @throws \Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException
     */
    public function update(CrudResource $entity, array $data): void {
        $this->validateEntity($entity, PaymentMethod::class);
        $entity->setName($data["name"]);
        $entity->setPrice($data["price"]);
        $entity->show();

        $this->save($entity);
    }
}
