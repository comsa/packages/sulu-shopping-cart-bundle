<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Comsa\SuluShoppingCart\Factory\TaxCategoryFactory;
use Comsa\SuluShoppingCart\Repository\TaxCategoryRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseOrmSearchInterface;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the TaxCategory class.
 * @package Comsa\SuluShoppingCart\Service
 */
class TaxCategoryService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, TaxCategoryRepository $repository) {
        parent::__construct($entityManager, $repository, TaxCategory::class);
    }

    /**
     * @param array $data
     * @return CrudResource
     */
    public function create(array $data): CrudResource {
        $entity = TaxCategoryFactory::create(
            title: $data["title"],
            percentage: $data["percentage"]
        );

        $this->save($entity);

        return $entity;
    }

    /**
     * @param TaxCategory $entity
     * @param array $data
     */
    public function update(CrudResource $entity, array $data): void {
        $this->validateEntity($entity);
        $entity->setTitle($data["title"]);
        $entity->setPercentage($data["percentage"]);
        $this->save($entity);
    }
}
