<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Comsa\SuluShoppingCart\Factory\ProductFactory;
use Comsa\SuluShoppingCart\Repository\ProductRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\MediaBundle\Entity\Media;

/**
 * CRUD Service for the Product Entity
 * @package Comsa\SuluShoppingCart\Service
 */
class ProductService extends AbstractCrudService implements BaseCrudServiceInterface {
    private OptionService $optionService;
    private TaxCategoryService $taxCategoryService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $repository,
        OptionService $optionService,
        TaxCategoryService $taxCategoryService
    ) {
        $this->optionService = $optionService;
        $this->taxCategoryService = $taxCategoryService;
        parent::__construct($entityManager, $repository, Product::class);
    }

    public function create(array $data): CrudResource {
        $product = ProductFactory::create(
            $data["code"],
            $data["title"],
            $data["description"],
            TypeConverter::stringToFloat((string) $data["price"]),
            (bool) $data["followStock"],
            (int) $data["stock"],
            $data["unit"],
            $data["paymentMethodType"],
            $data["subscriptionInterval"],
            $data["subscriptionAmount"],
            $data["timeLimitedAvailability"],
            $this->getDateTimeObject($data["startTimeAvailability"]),
            $this->getDateTimeObject($data["endTimeAvailability"]),
            $data["optionSelectionType"],
            $this->getThumbnail($data["thumbnail"]),
            $data["parentPage"],
            $this->optionService->map($data["productOptions"]),
            $this->getTaxCategory($data["taxCategory"])
        );

        $this->save($product);

        return $product;
    }

    /**
     * @param Product $entity
     * @param array $data
     */
    public function update(CrudResource $entity, array $data): void {
        $entity
            ->setCode($data["code"])
            ->setTitle($data["title"])
            ->setDescription($data["description"])
            ->setPrice(TypeConverter::stringToFloat((string) $data["price"]))
            ->setFollowStock((bool) $data["followStock"])
            ->setStock((int) $data["stock"])
            ->setUnit($data["unit"])
            ->setPaymentMethodType($data["paymentMethodType"])
            ->setSubscriptionInterval($data["subscriptionInterval"])
            ->setSubscriptionAmount($data["subscriptionAmount"])
            ->setTimeLimitedAvailbility($data["timeLimitedAvailability"])
            ->setStartTimeAvailability($this->getDateTimeObject($data["startTimeAvailability"]))
            ->setEndTimeAvailability($this->getDateTimeObject($data["endTimeAvailability"]))
            ->setOptionSelectionType($data["optionSelectionType"])
            ->setThumbnail($this->getThumbnail($data["thumbnail"]))
            ->setParentPage($data["parentPage"])
            ->setOptions($this->optionService->cleanOptions(
                $entity, $data["productOptions"]
            ))
            ->setTaxCategory($this->getTaxCategory($data["taxCategory"]))
        ;

        $this->save($entity);
    }

    public function updatePage(Product $entity, string $uuid): void {
        $entity->setPageId($uuid);
        $this->save($entity);
    }

    private function getDateTimeObject(?string $time): ?\DateTime {
        if ($time === null) {
            return null;
        }

        return new \DateTime($time);
    }

    private function getThumbnail(?array $image): ?Media {
        if ($image === null || !isset($image["id"]) || $image["id"] === null) {
            return null;
        }

        return $this->entityManager->getRepository(Media::class)->find($image["id"]);
    }

    private function getTaxCategory(null|int|array $id): ?TaxCategory {
        if (is_array($id)) {
            $id = $id["id"];
        }

        if ($id === null) {
            return null;
        }

        return $this->taxCategoryService->getRepository()->find($id);
    }
}
