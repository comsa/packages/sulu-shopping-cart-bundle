<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\ProductOption;
use Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException;
use Comsa\SuluShoppingCart\Factory\OptionFactory;
use Comsa\SuluShoppingCart\Factory\ProductOptionFactory;
use Comsa\SuluShoppingCart\Repository\OptionRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseOrmManagementInterface;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD service for the Option class
 * @package Comsa\SuluShoppingCart\Service\OptionService
 */
class OptionService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, OptionRepository $optionRepository) {
        parent::__construct($entityManager, $optionRepository, Option::class);
    }

    /**
     * @param array $data
     * @return CrudResource
     * @throws InvalidCrudResourceException
     */
    public function create(array $data): CrudResource {
        $entity = OptionFactory::create(
            title: $data["title"],
            price: $data["price"]
        );

        $this->save($entity);

        return $entity;
    }

    /**
     * @param Option $entity
     * @param array $data
     * @throws InvalidCrudResourceException
     */
    public function update(CrudResource $entity, array $data): void {
        $entity->setTitle($data["title"]);
        $entity->setPrice($data["price"]);

        $this->save($entity);
    }

    public function map(?array $data): Collection {
        $optionPosition = 0;
        $options = new ArrayCollection();

        if ($data === null) {
            return $options;
        }

        foreach ($data as $option) {
            $optionObject = $this->getRepository()->findOneByTitleAndPrice(
                $option["optionTitle"],
                TypeConverter::stringToFloat((string) $option["optionPrice"])
            );

            if ($optionObject === null) {
                $optionObject = $this->create([
                    "title" => $option["optionTitle"],
                    "price" => TypeConverter::stringToFloat((string) $option["optionPrice"])
                ]);
            }

            $options->add(
                ProductOptionFactory::create(
                    null,
                    $optionObject,
                    $optionPosition
                )
            );

            $optionPosition++;
        }

        return $options;
    }

    public function cleanOptions(Product $product, ?array $data): Collection {
        $existingOptions = [];
        $newOptions = new ArrayCollection();
        $newProductOptions = new ArrayCollection();

        foreach ($product->getOptions() as $existingOption) {
            $existingOptions[$existingOption->getId()] = $existingOption->__toString();
        }

        if (is_array($data)) {
            $optionPosition = 0;
            foreach ($data as $option) {
                if (!in_array(
                    sprintf("%s%d", $option["optionTitle"], $option["optionPrice"]),
                    $existingOptions
                )) {
                    $optionEntity = $this->getRepository()->findOneByTitleAndPrice(
                        $option["optionTitle"],
                        TypeConverter::stringToFloat((string) $option["optionPrice"])
                    );

                    if ($optionEntity === null) {
                        $optionEntity = $this->create([
                            "title" => $option["optionTitle"],
                            "price" => TypeConverter::stringToFloat((string) $option["optionPrice"])
                        ]);
                    }

                    $productOption = ProductOptionFactory::create(
                        $product,
                        $optionEntity,
                        $optionPosition
                    );

                    $newProductOptions->add($productOption);
                } else {
                    $id = array_search(
                        sprintf(
                            "%s%d", $option["optionTitle"], $option["optionPrice"]
                        ),
                        $existingOptions
                    );

                    $optionToUpdate = $this->entityManager->getRepository(ProductOption::class)->find($id);
                    $optionToUpdate->setPosition($optionPosition);
                    $newProductOptions->add($optionToUpdate);
                }

                $newOptions->add(sprintf(
                   "%s%d", $option["optionTitle"], $option["optionPrice"]
                ));

                $optionPosition++;
            }

            foreach ($existingOptions as $key => $existingOption) {
                if (!$newOptions->contains($existingOption)) {
                    $optionObject = $this->entityManager->getRepository(ProductOption::class)->find($key);
                    $product->removeOption($optionObject);
                    $this->entityManager->remove($optionObject);
                }
            }

            return $newProductOptions;
        }
    }
}
