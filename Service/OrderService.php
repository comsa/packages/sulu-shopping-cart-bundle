<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Service;

use Comsa\SuluShoppingCart\Entity\Interfaces\CrudResource;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Repository\OrderRepository;
use Comsa\SuluShoppingCart\Service\Interfaces\BaseOrmSearchInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the Order class
 * @package Comsa\SuluShoppingCart\Service
 */
class OrderService extends AbstractCrudService {
    public function __construct(EntityManagerInterface $entityManager, OrderRepository $orderRepository) {
        parent::__construct($entityManager, $orderRepository, Order::class);
    }

    /**
     * @param Order $order
     */
    public function setPaid(Order $order): void {
        $order->getPayment()->setPaid();
        $this->entityManager->flush();
    }
}
