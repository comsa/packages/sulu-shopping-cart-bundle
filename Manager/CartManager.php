<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Manager;

use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\CartItem;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\ShippingMethod;
use Doctrine\ORM\EntityManagerInterface;
use Massive\Bundle\SearchBundle\Tests\Resources\TestBundle\Entity\Car;
use Sulu\Component\Content\Document\Structure\ManagedStructure;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Manages the Cart
 * @package Comsa\SuluShoppingCart\Manager
 */
class CartManager {
    private SessionInterface $session;
    private EntityManagerInterface $entityManager;
    private DocumentManagerInterface $documentManager;
    private ParameterBagInterface $parameterBag;
    private RouterInterface $router;

    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager, DocumentManagerInterface $documentManager, ParameterBagInterface $parameterBag, RouterInterface $router) {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->documentManager = $documentManager;
        $this->parameterBag = $parameterBag;
        $this->router = $router;
    }

    public function create(): Cart {
        $cart = new Cart();
        $this->session->set('cart', $cart);
        return $cart;
    }

    public function save(Cart $cart) {
        $this->entityManager->persist($cart);
        $this->entityManager->flush();

        $this->session->set('cart', $cart);
    }

    public function get(bool $forceNew = false): Cart {
        if ($this->session->has('cart') && $this->session->get('cart') instanceof Cart && $this->session->get('cart')->getId() && !$forceNew) {
            $cart = $this->entityManager->getRepository(Cart::class)->find($this->session->get('cart')->getId());
            if ($cart instanceof Cart) {
                return $cart;
            }
        }

        //-- Create a new one
        return $this->create();
    }

    public function setCartItemData(CartItem $cartItem) {
        /** @var ManagedStructure $structure */
        $structure = $this->documentManager->find($cartItem->getUuid())->getStructure();

        $product = $this->entityManager->find(Product::class ,$structure->getProperty('product')->getValue());
        $cartItem->setTitle($product->getTitle());
        $cartItem->setPrice((float) $product->getPrice());
        $cartItem->setProduct($product);
    }

    public function getAvailableShippingMethods(): array {
        return $this->entityManager->getRepository(ShippingMethod::class)->findAll();
    }

    public function getAvailablePaymentMethods(): array {
        return $this->entityManager->getRepository(PaymentMethod::class)->findAll();
    }

    public function nextStep(int $step): Response {
        switch ($step) {
            case 1:
                $step = 2;
            case 2:
                if (!$this->get()->getShippingMethod() && $this->parameterBag->get('comsa_sulu_shopping_cart_shipping') && !empty($this->getAvailableShippingMethods())) {
                    return new RedirectResponse($this->router->generate('comsa_sulu_shopping_cart_select_shipping'));
                }
            case 3:
                if (!$this->get()->getPaymentMethod() && $this->parameterBag->get('comsa_sulu_shopping_cart_payment') && !empty($this->getAvailablePaymentMethods())) {
                    return new RedirectResponse($this->router->generate('comsa_sulu_shopping_cart_select_payment'));
                }
            default:
                return new RedirectResponse($this->router->generate('comsa_sulu_shopping_cart_overview'));
        }
    }

    public function emptyCart(CartItem $cartItem) :void {
        $cartItem = $this->entityManager->getRepository(CartItem::class)->find($cartItem->getId());

        //-- Remove item
        $this->entityManager->remove($cartItem);
        $this->entityManager->flush();
    }
}
