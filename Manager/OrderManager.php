<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Manager;

use Comsa\SuluShoppingCart\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Manager Order
 * @package Comsa\SuluShoppingCart\Manager
 */
class OrderManager {
    
    private SessionInterface $session;
    private EntityManagerInterface $entityManager;

    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function get(): ?Order
    {
        if ($this->session->has('order') && $this->session->get('order') instanceof Order) {
            return $this->entityManager->getRepository(Order::class)->find($this->session->get('order')->getId());
        }
        return null;
    }

    public function save(Order $order) {
        //-- Save in DB
        $this->entityManager->persist($order);
        $this->entityManager->flush();

        //-- Save in session
        $this->session->set('order', $order);

        //-- Clear cart from session
        $this->session->remove('cart');
    }
}
