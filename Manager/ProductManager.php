<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Manager;

use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Comsa\SuluShoppingCart\Factory\ProductFactory;
use Doctrine\Common\Collections\Collection;
use Sulu\Bundle\MediaBundle\Entity\Media;

/**
 * Delegates the Creation and Updating of a Product
 */
class ProductManager {

    public function create(
        ?string $code,
        ?string $title,
        ?string $description,
        ?float $price,
        bool $followStock,
        ?int $stock,
        string $unit,
        string $paymentMethodType,
        ?string $subscriptionInterval,
        ?int $subscriptionAmount,
        ?bool $timeLimitedAvailability,
        ?\DateTime $startTimeAvailability,
        ?\DateTime $endTimeAvailability,
        ?string $optionSelectionType,
        ?Media $thumbnail,
        string|PageDocument $parent,
        Collection $options,
        ?TaxCategory $taxCategory
    ): Product {
        return ProductFactory::create(
            code: $code,
            title: $title,
            description: $description,
            price: $price,
            followStock: $followStock,
            stock: $stock,
            unit: $unit,
            paymentMethodType: $paymentMethodType,
            subscriptionInterval: $subscriptionInterval,
            subscriptionAmount: $subscriptionAmount,
            timeLimitedAvailability: $timeLimitedAvailability,
            startTimeAvailability: $startTimeAvailability,
            endTimeAvailability: $endTimeAvailability,
            optionSelectionType: $optionSelectionType,
            thumbnail: $thumbnail,
            parent: $parent,
            options: $options,
            taxCategory: $taxCategory
        );
    }

    public function update(
        Product $product,
        ?string $code,
        ?string $title,
        ?string $description,
        ?float $price,
        bool $followStock,
        ?int $stock,
        string $unit,
        string $paymentMethodType,
        ?string $subscriptionInterval,
        ?int $subscriptionAmount,
        ?bool $timeLimitedAvailability,
        ?\DateTime $startTimeAvailability,
        ?\DateTime $endTimeAvailability,
        ?string $optionSelectionType,
        ?Media $thumbnail,
        string|PageDocument $parent,
        Collection $options,
        ?TaxCategory $taxCategory
    ): Product {
        return $product
            ->setCode($code)
            ->setTitle($title)
            ->setDescription($description)
            ->setPrice($price)
            ->setFollowStock($followStock)
            ->setStock($stock)
            ->setUnit($unit)
            ->setPaymentMethodType($paymentMethodType)
            ->setSubscriptionInterval($subscriptionInterval)
            ->setSubscriptionAmount($subscriptionAmount)
            ->setTimeLimitedAvailbility($timeLimitedAvailability)
            ->setStartTimeAvailability($startTimeAvailability)
            ->setEndTimeAvailability($endTimeAvailability)
            ->setOptionSelectionType($optionSelectionType)
            ->setThumbnail($thumbnail)
            ->setParentPage($parent)
            ->setOptions($options)
            ->setTaxCategory($taxCategory)
        ;
    }
}
