<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Form;

use Comsa\SuluShoppingCart\Entity\CartItem;
use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\ProductOption;
use Comsa\SuluShoppingCart\Modal\AddToCart;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Config\Definition\Exception\DuplicateKeyException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * Form for Add To Cart
 * @package Comsa\SuluShoppingCart\Form
 */
class AddToCartType extends AbstractType {

    private RouterInterface $router;

    public function __construct(RouterInterface $router) {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->setAction($this->router->generate("comsa_sulu_shopping_cart_add_to_cart"));
        $builder->setMethod("POST");
        $builder->add("title", HiddenType::class);
        $builder->add("price", HiddenType::class);

        if ($options["product"]->hasOptions())
        {
            if ($options["product"]->getOptionSelectionType() === Option::OPTION_SELECTION_TYPE_MULTIPLE) {
                $builder->add("options", EntityType::class, [
                    "class" => ProductOption::class,
                    "expanded" => true,
                    "multiple" => true,
                    "choice_label" => function($productOption, $key, $index) {
                        return $productOption->getOption()->getTitle() . " (€" . $productOption->getOption()->getPrice() . ")";
                    },
                    "label" => "lbl.Options",
                    "choices" => $options["product"]->getOptions()
                ]);
            }
            else {
                $builder->add("singleOption", EntityType::class, [
                    "class" => ProductOption::class,
                    "multiple" => false,
                    "required" => false,
                    "choice_label" => function($productOption, $key, $index) {
                        return $productOption->getOption()->getTitle() . " (€" . $productOption->getOption()->getPrice() . ")";
                    },
                    "label" => "comsa_sulu_shopping_cart.options",
                    "choices" => $options["product"]->getOptions()
                ]);
            }
        }

        $builder->add("quantity", IntegerType::class, [
            "attr" => ["class" => "form-control"],
            "label" => "comsa_sulu_shopping_cart.quantity"
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            "data_class" => \App\Entity\CartItem::class
        ]);

        $resolver->setRequired([
            "product",
            "options"
        ]);
    }
}
