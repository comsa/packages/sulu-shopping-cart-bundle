<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Form;

use Comsa\SuluShoppingCart\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form for Order
 * @package Comsa\SuluShoppingCart\Form
 */
class OrderType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if ($options["freeDateInput"]) {
            $builder
                ->add("deliveryDate", DateTimeType::class, [
                    "label" => "comsa_sulu_shopping_cart.delivery_date",
                    "required" => true,
                    "widget" => "single_text",
                    "html5" => true,
                    "attr" => [
                        "min" => $options["minDate"]
                    ]
                ]
            );
        }

        $builder
            ->add('note', TextareaType::class, [
                'required' => false,
                'label' => 'comsa_sulu_shopping_cart.note'
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            "freeDateInput"=> false,
            "minDate" => (new \DateTime())->format("Y-m-d"),
        ]);
    }
}
