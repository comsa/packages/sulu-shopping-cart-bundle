<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Form;

use Comsa\SuluShoppingCart\Entity\Mandate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form for Mandate
 * @package Comsa\SuluShoppingCart\Form
 */
class MandateType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add("consumerAccount", TextType::class, [
                "label" => "comsa_sulu_shopping_cart.consumer_account",
                "required" => true
            ])
            ->add("consumerBIC", TextType::class, [
                "label" => "comsa_sulu_shopping_cart.consumer_bic",
                "required" => true
            ])
            ->add("permission", CheckboxType::class, [
                "label" => "comsa_sulu_shopping_cart.consumer_permission",
                "required" => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
           "data_class" => Mandate::class
        ]);
    }
}
