<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Form;

use Comsa\SuluShoppingCart\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form for Customer
 * @package Comsa\SuluShoppingCart\Form
 */
class CustomerType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'comsa_sulu_shopping_cart.email',
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.phone_number',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.first_name'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.last_name'
            ])
            ->add('billingAddress', AddressType::class, [
                'label' => 'comsa_sulu_shopping_cart.billing_address'
            ])
            ->add('shippingAddress', AddressType::class, [
                'required' => false,
                'label' => 'comsa_sulu_shopping_cart.shipping_address'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Customer::class
        ]);
    }
}
