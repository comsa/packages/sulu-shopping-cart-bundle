<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Form;

use Comsa\SuluShoppingCart\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form Type for Address
 * @package Comsa\SuluShoppingCart\Form
 */
class AddressType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('street', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.street'
            ])
            ->add('streetNumber', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.street_number'
            ])
            ->add('postal', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.postal'
            ])
            ->add('city', TextType::class, [
                'label' => 'comsa_sulu_shopping_cart.city'
            ])
            ->add('country', CountryType::class, [
                'label' => 'comsa_sulu_shopping_cart.country'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Address::class
        ]);
    }
}
