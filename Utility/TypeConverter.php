<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Utility;

class TypeConverter
{
    public static function stringToFloat(string $string): float
    {
        if (str_contains($string, ",")) {
            $string = str_replace(",", '.', $string);
        }

        return (float) $string;
    }

    public static function stringToInt(string $string): int
    {
        return (int) $string;
    }

    public static function floatToString(float $float): string
    {
        $string = number_format($float, 2, '.', '');
        return $string;
    }

    public static function stringToPercentage(string $string): float {
        $percentage = self::stringToFloat($string);

        return (float) ($percentage/100);
    }
}
