<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Controller\Admin;

use Comsa\SuluShoppingCart\Entity\Address;
use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\Customer;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\ShippingMethod;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\TestBundle\Testing\SuluTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class OrderControllerTest extends SuluTestCase
{
    private KernelBrowser $client;
    private OrderRepository $orderRepository;
    private EntityManagerInterface $entityManager;

    private Address $address;
    private Customer $customer;
    private ShippingMethod $shippingMethod;
    private PaymentMethod $paymentMethod;
    private Cart $cart;
    private Order $order;
    private Payment $payment;

    public function setUp(): void {
        $this->client = self::createAuthenticatedClient();
        $this->orderRepository = self::getContainer()->get("comsa.sc.repository.order");
        $this->entityManager = self::getContainer()->get("doctrine.orm.entity_manager");

        $this->createAddress();
        $this->createCustomer();
        $this->createShippingMethod();
        $this->createPaymentMethod();
        $this->createCart();
        $this->createOrder();
        $this->createPayment();

        $this->entityManager->flush();
    }

    public function test_cgetAction_returnsOKResponse(): void {
        $this->client->request(
            method: "GET",
            uri: "/admin/api/orders"
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(200);
    }

    public function test_getAction_returnsOKResponse(): void {
        $this->client->request(
            method: "GET",
            uri: "/admin/api/orders/{$this->order->getId()}"
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(200);
    }

    public function test_getAction_unknownOrder_returnsNotFoundResponse(): void {
        $this->client->request(
            method: "GET",
            uri: "/admin/api/orders/600000000"
        );

        self::assertResponseStatusCodeSame(404);
    }

    public function test_deleteAction_returnsOKResponse(): void {
        $id = $this->order->getId();

        $this->client->request(
            method: "DELETE",
            uri: "/admin/api/orders/{$id}"
        );

        self::assertNull(
            $this->orderRepository->find($id)
        );
    }

    public function test_deleteAction_unknownProduct_returnsNotFoundResponse(): void {
        $this->client->request(
            method: "DELETE",
            uri: "/admin/api/orders/50000000"
        );

        self::assertResponseStatusCodeSame(404);
    }

    public function test_putPaidAction_setsOrderPaid(): void {
        $id = $this->order->getId();

        $this->client->request(
            method: "PUT",
            uri: "/admin/api/orders/{$id}/paid"
        );

        self::assertTrue(
            $this->orderRepository->find($id)->isPaid()
        );
    }

    public function test_putPaidAction_unknownOrder_returnsNotFoundResponse(): void {
        $this->client->request(
            method: "PUT",
            uri: "/admin/api/orders/50000000000/paid"
        );

        self::assertResponseStatusCodeSame(404);
    }

    private function createAddress(): void {
        $this->address = (new Address())
            ->setStreet("Test street")
            ->setStreetNumber("123456789")
            ->setPostal("8432")
            ->setCity("Test City")
            ->setCountry("BE")
        ;

        $this->entityManager->persist($this->address);
    }

    private function createCustomer(): void {
        $this->customer = (new Customer())
            ->setFirstName("John")
            ->setLastName("Doe")
            ->setEmail("JohnDoe@gmail.com")
            ->setMollieId("cst_1561651")
            ->setPhoneNumber("123456789")
            ->setBillingAddress($this->address)
            ->setShippingAddress($this->address)
        ;

        $this->entityManager->persist($this->customer);
    }

    private function createShippingMethod(): void {
        $this->shippingMethod = (new ShippingMethod())
            ->setType(SettingEnum::VALUES_SHIPPING_METHOD_PICKUP)
            ->setName("Test pickup shipping method")
            ->setPrice(1.00)
        ;

        $this->entityManager->persist($this->shippingMethod);
    }

    private function createPaymentMethod(): void {
        $this->paymentMethod = (new PaymentMethod())
            ->setType(SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE)
            ->setName("Test Mollie Payment")
            ->setPrice(1.00)
        ;

        $this->entityManager->persist($this->paymentMethod);
    }

    private function createCart(): void {
        $this->cart = (new Cart())
            ->setCustomer($this->customer)
            ->setPaymentMethod($this->paymentMethod)
            ->setShippingMethod($this->shippingMethod)
        ;

        $this->entityManager->persist($this->cart);
    }

    private function createOrder() : void {
        $this->order = (new Order())
            ->setCart($this->cart)
            ->setDeliveryDate(null)
            ->setNote("Test Note")
        ;

        $this->entityManager->persist($this->order);
    }

    private function createPayment(): void {
        $this->payment = (new Payment())
            ->setPaymentMethod($this->paymentMethod)
            ->setAmount(15.00)
            ->setPaymentMethodType(SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE)
            ->setOrder($this->order)
        ;
    }

    protected function tearDown(): void {
        self::ensureKernelShutdown();
    }
}

