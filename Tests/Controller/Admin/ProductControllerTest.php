<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Controller\Admin;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Enum\ProductEnum;
use Comsa\SuluShoppingCart\Factory\ProductFactory;
use Comsa\SuluShoppingCart\Repository\ProductRepository;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\False_;
use Ramsey\Uuid\Guid\Guid;
use Ramsey\Uuid\Uuid;
use Sulu\Bundle\TestBundle\Testing\SuluTestCase;
use Sulu\Component\Security\Authentication\UserRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductControllerTest extends SuluTestCase {
    private KernelBrowser $client;
    private EntityManagerInterface $entityManager;
    private Product $product;
    private ProductRepository $productRepository;

    public function setUp(): void {
        $this->client = self::createAuthenticatedClient();
        $this->entityManager = self::getContainer()->get("doctrine.orm.entity_manager");
        $this->productRepository = self::getContainer()->get("comsa.sc.repository.product");

        $this->product = ProductFactory::create(
            code: null,
            description: null,
            title: "Test Product FUNTIONAL TEST",
            price: 12.08,
            taxCategory: null,
            thumbnail: null,
            parent: Uuid::uuid4()->toString(),
            options: new ArrayCollection(),
            optionSelectionType: Option::OPTION_SELECTION_TYPE_SINGLE,
            endTimeAvailability: null,
            startTimeAvailability: null,
            timeLimitedAvailability: false,
            subscriptionAmount: null,
            subscriptionInterval: null,
            paymentMethodType: PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT,
            unit: ProductEnum::UNIT_EACH,
            stock: 0,
            followStock: false
        );
    }

    public function test_cgetAction_returnsOKResponse(): void {
        $this->client->request(
            method: "GET",
            uri: "/admin/api/products"
        );

        self::assertResponseIsSuccessful();
    }

    public function test_getAction_returnsOKResponse(): void {
        $this->entityManager->persist($this->product);
        $this->entityManager->flush();

        $this->client->request(
          method: "GET",
          uri: "/admin/api/products/{$this->product->getId()}"
        );

        self::assertResponseIsSuccessful();

        $this->entityManager->remove($this->product);
        $this->entityManager->flush();
    }

    public function test_getAction_unknownProduct_returnsNotFoundResponse(): void {
        $this->client->request(
            method: "GET",
            uri: "/admin/api/products/6000000000"
        );
        self::assertResponseStatusCodeSame(404);
    }

    public function test_postAction_addsCorrectProduct() {
        $this->client->request(
            "POST",
            "/admin/api/products",
            [
                "code" => $this->product->getCode(),
                "title" => $this->product->getTitle(),
                "description" => $this->product->getDescription(),
                "price" => (string) $this->product->getPrice(),
                "followStock" => $this->product->getFollowStock(),
                "stock" => $this->product->getStock(),
                "unit" => $this->product->getUnit(),
                "paymentMethodType" => $this->product->getPaymentMethodType(),
                "optionSelectionType" => $this->product->getOptionSelectionType(),
                "parentPage" => $this->product->getParentPage(),
                "productOptions" => [
                    [
                        "optionTitle" => "Option 1 FUNCTIONAL TEST",
                        "optionPrice" => "11.99"
                    ],
                    [
                        "optionTitle" => "Option 2 FUNCTIONAL TEST",
                        "optionPrice" => "15.99"
                    ]
                ],
            ]
        );

        $newProduct = $this->productRepository->findLatest();

        self::assertEquals(
            $this->product->getTitle(),
            $newProduct->getTitle()
        );

        $this->entityManager->remove($newProduct);
        $this->entityManager->flush();
    }

    public function test_deleteAction_returnsOKResponse() {
        $this->entityManager->persist($this->product);
        $this->entityManager->flush();

        $this->client->request(
            method:"DELETE",
            uri: "/admin/api/products/{$this->product->getId()}"
        );

        $newestProduct = $this->productRepository->findLatest();

        self::assertResponseIsSuccessful();
        self::assertNull($newestProduct);
    }

    public function test_deleteAction_unknownProduct_returnsNotFoundResponse(): void {
        $this->client->request(
          method: "DELETE",
          uri: "/admin/api/products/5000000000000"
        );

        self::assertResponseStatusCodeSame(404);
    }
}
