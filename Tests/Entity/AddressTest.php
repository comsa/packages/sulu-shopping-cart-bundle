<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Entity;

use Comsa\SuluShoppingCart\Entity\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase {
    public function test_getAddress_ReturnsExpectedString() {
        $street = "Test Street";
        $streetNumber = "80 0003";
        $postal = "8432";
        $city = "Test City";
        $country = "BE";

        $address = new Address();
        $address->setStreetNumber($streetNumber);
        $address->setPostal($postal);
        $address->setStreet($street);
        $address->setCity($city);
        $address->setCountry($country);

        $expectedString = sprintf(
          "%s %s, %s %s",
            $street,
            $streetNumber,
            $postal,
            $city,
            $country
        );

        self::assertEquals(
            $expectedString,
            $address->getAddress()
        );
    }
}
