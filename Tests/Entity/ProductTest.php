<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Entity;

use Comsa\SuluShoppingCart\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase {
    public function test_isAvailable_noChecksNeeded_returnsTrue(): void {
        $product = new Product();
        $product->setTimeLimitedAvailbility(false);
        $product->setFollowStock(false);

        self::assertTrue($product->isAvailable());
    }

    public function test_isAvailable_noStock_returnsFalse(): void {
        $product = new Product();
        $product->setTimeLimitedAvailbility(false);
        $product->setFollowStock(true);
        $product->setStock(0);

        self::assertFalse($product->isAvailable());
    }

    public function test_isAvailable_hasStock_returnsTrue(): void {
        $product = new Product();
        $product->setTimeLimitedAvailbility(false);
        $product->setFollowStock(true);
        $product->setStock(5);

        self::assertTrue($product->isAvailable());
    }

    public function test_isAvailable_limitedEdition_dateSmallerThanStart_returnsFalse(): void {
        $product = new Product();
        $product->setFollowStock(false);
        $product->setTimeLimitedAvailbility(true);
        $product->setStartTimeAvailability((new \DateTime())->modify("+2 days"));
        $product->setEndTimeAvailability((new \DateTime())->modify("+10 days"));

        self::assertFalse($product->isAvailable());
    }

    public function test_isAvailable_limitedEdition_dateLargerThanEnd_returnsFalse(): void {
        $product = new Product();
        $product->setFollowStock(false);
        $product->setTimeLimitedAvailbility(true);
        $product->setStartTimeAvailability((new \DateTime())->modify("-10 days"));
        $product->setEndTimeAvailability((new \DateTime())->modify("-2 days"));

        self::assertFalse($product->isAvailable());
    }

    public function test_isAvailable_limitedEdition_dateEqualToStart_returnsTrue(): void {
        $product = new Product();
        $product->setFollowStock(false);
        $product->setTimeLimitedAvailbility(true);
        $product->setStartTimeAvailability((new \DateTime())->setTime(0,0,0));
        $product->setEndTimeAvailability((new \DateTime())->modify("+5 days"));

        self::assertTrue($product->isAvailable());
    }

    public function test_isAvailable_limitedEdition_dateEqualToEnd_returnsTrue(): void {
        $product = new Product();
        $product->setFollowStock(false);
        $product->setTimeLimitedAvailbility(true);
        $product->setStartTimeAvailability((new \DateTime())->modify("-2 days"));
        $product->setEndTimeAvailability((new \DateTime())->setTime(23,59,59));

        self::assertTrue($product->isAvailable());
    }

    public function test_isAvailable_limitedEdition_dateBetweenStartAndEnd_returnsTrue(): void {
        $product = new Product();
        $product->setFollowStock(false);
        $product->setTimeLimitedAvailbility(true);
        $product->setStartTimeAvailability((new \DateTime())->modify("-5 days"));
        $product->setEndTimeAvailability((new \DateTime())->modify("+5 days"));

        self::assertTrue($product->isAvailable());
    }
}
