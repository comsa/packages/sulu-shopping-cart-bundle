<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Validator;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Enum\ProductEnum;
use Comsa\SuluShoppingCart\Validator\Request\PostProductValidator;
use PHPUnit\Framework\TestCase;

class PostProductValidatorTest extends TestCase {
    public function test_validate_returnsEmptyViolations(): void {
        $violations = PostProductValidator::validate([
            "title" => "Test Product",
            "price" => "16.00",
            "paymentMethodType" => PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT,
            "unit" => ProductEnum::UNIT_EACH,
            "optionSelectionType" => Option::OPTION_SELECTION_TYPE_SINGLE
        ]);

        self::assertEmpty($violations);
    }

    public function test_validate_returnsFiveViolations(): void {
        $violations = PostProductValidator::validate([
            "title" => "",
            "price" => "0.00",
            "paymentMethodType" => "unknown",
            "unit" => "unknown",
            "optionSelectionType" => "unknown"
        ]);

        self::assertCount(
            5,
            $violations
        );
    }
}
