<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Service;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException;
use Comsa\SuluShoppingCart\Factory\OptionFactory;
use Comsa\SuluShoppingCart\Service\OptionService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OptionServiceTest extends KernelTestCase {
    private OptionService $service;
    private EntityManagerInterface $entityManager;

    public function setUp(): void {
        self::bootKernel();
        $this->service = self::getContainer()->get("comsa.sc.service.option");
        $this->entityManager = self::getContainer()->get("doctrine.orm.entity_manager");
    }

    public function test_create_returnsExpectedOption(): void {
        // Arrange
        $title = Uuid::uuid4()->toString();
        $price = $this->generateRandomPrice();

        // Act
        /** @var Option $created */
        $created = $this->service->create([
            "title" => $title,
            "price" => $price
        ]);
        /** @var Option $lastPersisted */
        $lastPersisted = $this->service->findAll(Option::class)->last();

        // Assert
        self::assertSame($title, $created->getTitle());
        self::assertSame($price, $created->getPrice());
        self::assertSame($created->getTitle(), $lastPersisted->getTitle());
        self::assertSame($created->getPrice(), $lastPersisted->getPrice());

        // Reset
        $this->removeEntity($lastPersisted);
    }

    public function test_update_returnsExpectedDate(): void {
        // Arrange
        $created = $this->createRandomOption();
        $this->entityManager->persist($created);
        $this->entityManager->flush();

        $newTitle = Uuid::uuid4()->toString();
        $newPrice = $this->generateRandomPrice();

        // Act
        $lastPersisted = (new ArrayCollection($this->entityManager->getRepository(Option::class)->findAll()))->last();
        $this->service->update($lastPersisted, [
            "title" => $newTitle,
            "price" => $newPrice
        ]);

        /** @var Option $lastPersisted */
        $lastPersisted = (new ArrayCollection($this->entityManager->getRepository(Option::class)->findAll()))->last();

        // Assert
        self::assertSame($newTitle, $lastPersisted->getTitle());
        self::assertSame($newPrice, $lastPersisted->getPrice());

        // Reset
        $this->removeEntity($lastPersisted);
    }

    public function test_update_throwsInvalidCrudResourceException(): void {
        // Assert
        $this->expectException(InvalidCrudResourceException::class);

        // Act
        $this->service->update(new Product(), [
            "title" => Uuid::uuid4()->toString(),
            "price" => $this->generateRandomPrice()
        ]);

        // Arrange

    }

    private function createRandomOption(): Option {
        return OptionFactory::create(
          Uuid::uuid4()->toString(),
            $this->generateRandomPrice()
        );
    }

    private function generateRandomPrice(): float {
        return mt_rand(0 * 10, 1000*10)/10;
    }

    private function removeEntity(Option $option): void {
        $this->entityManager->remove($option);
        $this->entityManager->flush();
    }
}
