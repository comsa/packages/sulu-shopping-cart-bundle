<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Tests\Service;

use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Repository\OrderRepository;
use Comsa\SuluShoppingCart\Service\OrderService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OrderServiceTest extends KernelTestCase {
    private EntityManagerInterface $entityManager;
    private OrderService $orderService;

    public function setUp(): void {
        self::bootKernel();
        $this->orderService = self::getContainer()->get("comsa.sc.service.order");
        $this->entityManager = self::getContainer()->get("doctrine.orm.entity_manager");
    }

    public function test_setPaid_returnsOrderIsPaid(): void {
        // Arrange
        $payment = new Payment();
        $payment->setAmount(15.00);
        $payment->setPaymentMethodType("online");
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $cart = new Cart();
        $this->entityManager->persist($cart);
        $this->entityManager->flush();

        $order = new Order();
        $order->setPayment($payment);
        $order->setCart(new Cart());
        $this->entityManager->persist($order);

        $this->entityManager->flush();

        // Act
        $this->orderService->setPaid($order);
        /** @var Order $lastPersisted */
        $lastPersisted = (new ArrayCollection(
            $this->entityManager->getRepository(Order::class)->findAll()
        ))->last();

        // Assert
        self::assertTrue($lastPersisted->isPaid());

        // Reset
        $this->entityManager->remove($payment);
        $this->entityManager->remove($cart);
        $this->entityManager->remove($order);
        $this->entityManager->flush();
    }
}
