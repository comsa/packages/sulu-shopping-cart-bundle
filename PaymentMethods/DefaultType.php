<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\PaymentMethods;

use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Event\OrderConfirmedEvent;
use Comsa\SuluShoppingCart\Event\OrderFulfilledEvent;
use Comsa\SuluShoppingCart\PaymentMethods\Interfaces\PaymentMethodInterface;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * Types that don't include Mollie
 * @package Comsa\SuluShoppingCart\PaymentMethods
 */
class DefaultType implements PaymentMethodInterface {
    private Environment $twig;
    private TemplateAttributeResolverInterface $templateAttributeResolver;
    private ParameterBagInterface $parameterBag;
    protected EventDispatcherInterface $eventDispatcher;
    private RouterInterface $router;
    private EntityManagerInterface $entityManager;
    private SettingRepository $settingRepository;

    public function __construct(
        Environment $twig,
        TemplateAttributeResolverInterface $templateAttributeResolver,
        ParameterBagInterface $parameterBag,
        EventDispatcherInterface $eventDispatcher,
        RouterInterface $router,
        EntityManagerInterface $entityManager,
        SettingRepository $settingRepository
    ) {
        $this->twig = $twig;
        $this->templateAttributeResolver = $templateAttributeResolver;
        $this->parameterBag = $parameterBag;
        $this->eventDispatcher = $eventDispatcher;
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->settingRepository = $settingRepository;
    }

    /**
     * Just render back the payment details of the site
     * @param Payment $payment
     * @return Response
     */
    public function handlePayment(Payment $payment): Response {
        //-- Dispatch order event
        $this->confirmOrder($payment->getOrder());

        return new RedirectResponse($this->router->generate('comsa_sulu_shopping_cart_thanks'));
    }

    public function getAdditionalTemplateData(Order $order): array {
        return [
            'transfer_details' => $order->getCart()->getPaymentMethod()->getType() !== SettingEnum::VALUES_PAYMENT_METHOD_CASH ? $this->getTransferDetails(): null,
            "thankYouText" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_ORDER_COMPLETED_TEXT)->getValue()
        ];
    }

    protected function getTransferDetails(): array {
        return [
            "iban" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_IBAN)->getValue(),
            "bic" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_BIC)->getValue(),
        ];
    }

    protected function confirmOrder(Order $order): void {
        $event = new OrderConfirmedEvent($order);
        $this->eventDispatcher->dispatch($event, OrderConfirmedEvent::NAME);
    }

    public function afterPayment(Order $order): Response {
        return new RedirectResponse($this->router->generate('comsa_sulu_shopping_cart_thanks'));
    }

    public function checkPayment(Payment $payment): bool {
        return false;
    }

    public function afterSuccessWebhook(Payment $payment): void {
        // Nothing has to be done here, there's no webhook by default
    }
}
