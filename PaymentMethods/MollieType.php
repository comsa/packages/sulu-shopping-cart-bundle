<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\PaymentMethods;

use Comsa\SuluShoppingCart\Entity\CartItem;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Comsa\SuluShoppingCart\Enum\ProductEnum;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Event\OrderConfirmedEvent;
use Comsa\SuluShoppingCart\Event\OrderFulfilledEvent;
use Comsa\SuluShoppingCart\PaymentMethods\Interfaces\PaymentMethodInterface;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Mollie\Api\MollieApiClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Handles Mollie payments
 * @package Comsa\SuluShoppingCart\PaymentMethods
 */
class MollieType {

    private ParameterBagInterface $parameterBag;
    private RouterInterface $router;
    private EntityManagerInterface $entityManager;
    private MollieApiClient $mollie;
    private EventDispatcherInterface $eventDispatcher;
    private SettingRepository $settingRepository;

    public function __construct(ParameterBagInterface $parameterBag, RouterInterface $router, EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, SettingRepository $settingRepository) {
        $this->parameterBag = $parameterBag;
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->settingRepository = $settingRepository;

        $this->mollie = new MollieApiClient();
        $this->mollie->setApiKey($this->parameterBag->get('comsa_sulu_shopping_mollie_api_key'));
    }

    public function getAdditionalTemplateData(Order $order): array {
        return [
            "thankYouText" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_ORDER_COMPLETED_TEXT)->getValue()
        ];
    }

    public function handlePayment(Payment $payment): Response {
        //-- Check if single Payment or Subscription
        /** @var Product $product */
        $product = $payment->getOrder()->getCart()->getItems()[0]->getProduct();

        if ($payment->getPaymentMethodType() === PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT) {
            //-- Create mollie payment from order
            $molliePayment = $this->mollie->payments->create([
                'amount' => [
                    'currency' => 'EUR',
                    'value' => TypeConverter::floatToString($payment->getAmount())
                ],
                'description' => $this->formatSinglePaymentDescription($payment, PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT),
                'redirectUrl' => $this->router->generate('comsa_sulu_shopping_cart_handle_payment_redirect', [], RouterInterface::ABSOLUTE_URL),
//                'webhookUrl' => 'https://webhook.site/f98d970c-f7aa-4886-9c27-9e33b5495d7c'
                'webhookUrl' => $this->router->generate('comsa_sulu_shopping_cart_webhook_payment',[], RouterInterface::ABSOLUTE_URL)
            ]);

            //-- Save payment ID to payment
            $payment->setExternalId($molliePayment->id);
            $this->entityManager->flush();

            //-- Redirect
            return new RedirectResponse($molliePayment->getCheckoutUrl());
        } else {
            //-- Create first payment from order
            $customer = $payment->getOrder()->getCart()->getCustomer();
            $mollieCustomer = $this->mollie->customers->get($customer->getMollieId());

            $firstPayment = $this->mollie->payments->create([
                "amount" => [
                    "currency" => "EUR",
                    "value" => TypeConverter::floatToString($payment->getAmount())
                ],
                "method" => "bancontact",
                "customerId" => $customer->getMollieId(),
                "sequenceType" => "first",
                "description" => $this->formatSinglePaymentDescription($payment, PaymentMethod::PAYMENT_METHOD_TYPE_SUBSCRIPTION),
                'redirectUrl' => $this->router->generate('comsa_sulu_shopping_cart_handle_payment_redirect', [], RouterInterface::ABSOLUTE_URL),
                "webhookUrl" => $this->router->generate('comsa_sulu_shopping_cart_webhook_payment',[], RouterInterface::ABSOLUTE_URL)
//                'webhookUrl' => 'https://webhook.site/f98d970c-f7aa-4886-9c27-9e33b5495d7c'
            ]);

            $payment->setExternalId($firstPayment->id);
            $this->entityManager->flush();

            //-- Redirect
            return new RedirectResponse($firstPayment->getCheckoutUrl());
        }
    }

    public function createSubscription(Order $order): void {
        $product = $order->getCart()->getItems()->first()->getProduct();
        $payment = $order->getPayment();
        $customer = $order->getCart()->getCustomer();
        $mollieCustomer = $this->mollie->customers->get($customer->getMollieId());

        switch ($product->getSubscriptionInterval()) {
            case ProductEnum::SUBSCRIPTION_INTERVAL_WEEKLY:
                $interval = "7 days";
                $startDate = (new \DateTime())->modify("+1 week");
                break;
            case ProductEnum::SUBSCRIPTION_INTERVAL_MONTHLY:
                $interval = "1 month";
                $startDate = (new \DateTime())->modify("+1 month");
                break;
            default:
                $interval = "12 months";
                $startDate = (new \DateTime())->modify("+1 year");
        }

        $subscription = $mollieCustomer->createSubscription([
            "amount" => [
                "currency" => "EUR",
                "value" => TypeConverter::floatToString($payment->getAmount())
            ],
            "times" => ($product->getSubscriptionAmount() - 1), //-- -1 because first payment already happened at this point,
            "interval" => $interval,
            "startDate" => $startDate->format("Y-m-d"),
            "description" => $this->formatSubscriptionDescription($payment),
//            'webhookUrl' => 'https://webhook.site/f98d970c-f7aa-4886-9c27-9e33b5495d7c'
            "webhookUrl" => $this->router->generate('comsa_sulu_shopping_cart_webhook_payment',[], RouterInterface::ABSOLUTE_URL)
        ]);

        $payment->setExternalSubscriptionId($subscription->id);

        $this->entityManager->flush();
    }

    public function afterPayment(Order $order): Response {
        if ($order->getPayment()->getPaymentMethodType() === PaymentMethod::PAYMENT_METHOD_TYPE_SUBSCRIPTION) {
            $this->createSubscription($order);
        }
        return new RedirectResponse($this->router->generate('comsa_sulu_shopping_cart_thanks'));
    }

    public function checkPayment(Payment $payment): bool {
        $molliePayment = $this->mollie->payments->get($payment->getExternalId());
        return $molliePayment->isPaid();
    }

    public function checkSubscription(Payment $payment): bool {
        $molliePayment = $this->mollie->payments->get($payment->getExternalSubscriptionId());
        return $molliePayment->isPaid();
    }

    public function isRefunded(Payment $payment): bool {
        $molliePayment = $this->mollie->payments->get($payment->getExternalId());
        return $molliePayment->hasRefunds();
    }

    public function afterSuccessWebhook(Payment $payment): void {
        $this->confirmOrder($payment->getOrder());
    }

    protected function confirmOrder(Order $order): void {
        $event = new OrderConfirmedEvent($order);
        $this->eventDispatcher->dispatch($event, OrderConfirmedEvent::NAME);
    }

    private function formatSinglePaymentDescription(Payment $payment, string $paymentMethod): string {
        $taxString = $this->getTaxString($payment);

        return sprintf(
            "Order #%d %s %s",
            $payment->getOrder()->getId(),
            $paymentMethod === PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT ? "via Eenmalige Betaling" : "eerste betaling",
            $taxString
        );
    }

    private function formatSubscriptionDescription(Payment $payment) {
        $taxString = $this->getTaxString($payment);

        return sprintf(
            "Order #%d via domicilieëring %s",
            $payment->getOrder()->getId(),
            $taxString
        );
    }

    private function getTaxString(Payment $payment): string {
        $taxCategories = new ArrayCollection();

        /** @var CartItem $cartItem */
        foreach ($payment->getOrder()->getCart()->getItems() as $cartItem) {
            $taxCategory = $cartItem->getProduct()->getTaxCategory();
            if ($taxCategory && !$taxCategories->contains($taxCategory)) {
                $taxCategories->add($taxCategory);
            }
        }

        $taxString = "(";

        /** @var TaxCategory $taxCategory */
        foreach ($taxCategories as $taxCategory) {
            $taxString .= ($taxCategory->getPercentage() * 100 . "%");

            if ($taxCategories->last() !== $taxCategory) {
                $taxString .= ", ";
            }
        }

        $taxString .= ")";

        return $taxString;
    }
}
