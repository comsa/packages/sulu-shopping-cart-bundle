<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\PaymentMethods\Interfaces;

use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Payment;
use Symfony\Component\HttpFoundation\Response;

interface PaymentMethodInterface
{
    public function getAdditionalTemplateData(Order $order): array;
    public function handlePayment(Payment $payment): Response;
    public function afterPayment(Order $order): Response;
    public function checkPayment(Payment $payment): bool;
    public function afterSuccessWebhook(Payment $payment): void;
}
