import React from 'react';
import { viewRegistry } from 'sulu-admin-bundle/containers/ViewRenderer';
import { fieldRegistry } from 'sulu-admin-bundle/containers';
import Order from "./views/Order/Order";
import initializer from 'sulu-admin-bundle/services/initializer';
import Field from 'sulu-admin-bundle/components/Form/Field';
import Input from 'sulu-admin-bundle/components/Input';
import {observer} from 'mobx-react';
import {action,observable} from 'mobx';
import {listToolbarActionRegistry} from 'sulu-admin-bundle/views';
import ExportOrders from './listToolbarActions/ExportOrders'

listToolbarActionRegistry.add('comsa_sulu_shopping_cart.export_orders', ExportOrders);

type Props = {|
  id?: string,
  name?: string,
  onChange: (value: ?string) => void,
  value: ?string,
|};

@observer
class Money extends React.Component<Props> {
  @observable value: ?string;

  @action setValue(value) {
    this.value = value;
  }
  componentDidMount() {
    let value = this.props.value;
    if (value) {
      value = parseFloat(value).toFixed(2);
    }
    this.setValue(value);
  }
  handlePriceChange = (value) => {
    this.setValue(value);
  }
  handleBlur = () => {
    let value = this.value;
    if (value) {
      value = parseFloat(value).toFixed(2);
    }
    this.setValue(value);
    this.props.onChange(value);
  }
  render() {
    return (
      <Input icon="su-euro" value={this.value} onChange={this.handlePriceChange} onBlur={this.handleBlur}/>
    )
  }
}

initializer.addUpdateConfigHook('sulu_admin', (config: Object, initialized: boolean) => {
  if (!initialized) {
    viewRegistry.add('sulu_shopping_cart.order', Order);
    fieldRegistry.add(
      'money', Money
    );
  }
});
