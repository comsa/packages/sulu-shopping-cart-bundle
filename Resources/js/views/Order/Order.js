import React from 'react';
import type {ViewProps} from 'sulu-admin-bundle/containers/ViewRenderer';
import {withToolbar} from 'sulu-admin-bundle/containers/Toolbar';
import {ResourceRequester} from 'sulu-admin-bundle/services';
import {action, computed, toJS, isObservableArray, observable} from 'mobx';
import Loader from 'sulu-admin-bundle/components/Loader';
import Section from 'sulu-admin-bundle/components/Form/Section'
import Table from 'sulu-admin-bundle/components/Table'
import Grid from 'sulu-admin-bundle/components/Grid'
import Card from 'sulu-admin-bundle/components/Card'
import Divider from 'sulu-admin-bundle/components/Divider'
import Address from './Address';
import Price from './Price';
import {translate} from 'sulu-admin-bundle/utils/Translator';
import orderStyles from './order.scss';
import axios from 'axios';
import symfonyRouting from 'fos-jsrouting/router';

type
Props = {
  ...ViewProps,
  title? : string,
};

class Order extends React.Component<Props> {
  @observable loading = false;

  constructor(props: Props) {
    super(props);
    this.state = {
      order: null
    }
  }

  @action
  async componentDidMount() {
    const {router} = this.props;
    const order = await ResourceRequester.get('orders', {id: router.attributes.id});
    this.setState({
      order
    })
  }

  @action
  async setPaid() {
    this.loading = true;

    const {router} = this.props;
    let id = router.attributes.id;

    const response = axios.put(symfonyRouting.generate('comsa_sulu_shopping_cart.put_order_paid', {id: id })).finally(action(() => {
      this.loading = false;
    }));

    window.location.reload();
  }

  @action
  async getOrder() {
    let {router} = this.props;
    let order = await ResourceRequester.get('orders', {id: router.attributes.id});

    return order;
  }

  @action
  calculateItemPrice(item) {
    let price = item.price;

    item.options.forEach(function (item) {
      price += item.price;
    })

    return price * item.quantity;
  }

  render() {
    const {order} = this.state;
    if (!order) {
      return <Loader/>
    }
    var baseCartItemProperties = ["id", "product", "uuid", "quantity", "title", "options", "price", "discr"];
    var cartItemProperties = [];
    order.cart.items.forEach(function (o) {
      Object.getOwnPropertyNames(o).forEach(function (p){
        if (!cartItemProperties.includes(p) && !baseCartItemProperties.includes(p)){
          cartItemProperties.push(p);
        }
      })
    });

    var baseOrderProperties = ["id", "cart", "status", "note", "payment", "createdAt", "updatedAt", "discr", "total"];
    var orderProperties = [];
    Object.getOwnPropertyNames(order).forEach(function (p){
      if (!orderProperties.includes(p) && !baseOrderProperties.includes(p)){
        orderProperties.push(p)
      }
    });

    var param = "";

    var totalPriceColspan = 5 + cartItemProperties.length;
    var shippingAndPaymentMethodColspan = 4 + cartItemProperties.length;
    return (
      <div>
        <Section label="Order">
          <Card>
            <h1>Order #{order.id}</h1>
            <h3>{translate('comsa_sulu_shopping_cart.order_details')}</h3>
            <Table skin="light">
              <Table.Header>
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.quantity')}</Table.HeaderCell>
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.product')}</Table.HeaderCell>
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.productPrice')}</Table.HeaderCell>
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.options')}</Table.HeaderCell>
                {cartItemProperties.map((p) => (
                  <Table.HeaderCell>{translate('properties.' + p)}</Table.HeaderCell>
                ))}
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.optionPrices')}</Table.HeaderCell>
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.total')}</Table.HeaderCell>
              </Table.Header>
              <Table.Body>
                {cartItemProperties.length > 0
                  ? cartItemProperties.map((p) => (
                    order.cart.items.map((item) => (
                      <Table.Row key={item.id}>
                        <Table.Cell>{item.quantity}</Table.Cell>
                        <Table.Cell>{item.title}</Table.Cell>
                        <Table.Cell><Price amount={item.price}/></Table.Cell>
                        <Table.Cell>
                        <ul style={{listStyle: 'none'}}>
                          {item.options.map((option) => (
                            <li>{option.title}</li>
                          ))}
                        </ul>
                        </Table.Cell>
                        <Table.Cell>{item[p]}</Table.Cell>
                        <Table.Cell>
                          <ul style={{listStyle: 'none'}}>
                            {item.options.map((option) => (
                              <li><Price amount={option.price}/></li>
                            ))}
                          </ul>
                        </Table.Cell>
                        <Price amount={this.calculateItemPrice(item)} />
                      </Table.Row>
                    ))
                  ))
                  : order.cart.items.map((item) => (
                      <Table.Row key={item.id}>
                        <Table.Cell>{item.quantity}</Table.Cell>
                        <Table.Cell>{item.title}</Table.Cell>
                        <Table.Cell><Price amount={item.price}/></Table.Cell>
                        <Table.Cell>
                          <ul style={{listStyle: 'none'}}>
                            {item.options.map((option) => (
                              <li>{option.title}</li>
                            ))}
                          </ul>
                        </Table.Cell>
                        <Table.Cell>
                          <ul style={{listStyle: 'none'}}>
                            {item.options.map((option) => (
                              <li><Price amount={option.price}/></li>
                            ))}
                          </ul>
                        </Table.Cell>
                        <Table.Cell>
                          <Price amount={this.calculateItemPrice(item)} />
                        </Table.Cell>
                      </Table.Row>
                    ))
                }
                <Table.Row>
                  <Table.Cell>
                    <strong>{translate('comsa_sulu_shopping_cart.shipping_method')}</strong>
                  </Table.Cell>
                  <Table.Cell colSpan={shippingAndPaymentMethodColspan}>
                    {order.cart.shippingMethod ? order.cart.shippingMethod.name : translate('comsa_sulu_shopping_cart.no_shipping_method_linked')}
                  </Table.Cell>
                  <Table.Cell>
                    <Price amount={order.cart.shippingMethod ? order.cart.shippingMethod.price : 0}/>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <strong>{translate('comsa_sulu_shopping_cart.payment_method')}</strong>
                  </Table.Cell>
                  <Table.Cell colSpan={shippingAndPaymentMethodColspan}>
                    {order.cart.paymentMethod ? order.cart.paymentMethod.name : translate('comsa_sulu_shopping_cart.no_payment_method_linked')}
                  </Table.Cell>
                  <Table.Cell>
                    <Price amount={order.cart.paymentMethod ? order.cart.paymentMethod.price : 0}/>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell colSpan={totalPriceColspan}>
                    <strong>{translate('comsa_sulu_shopping_cart.total')}</strong>
                  </Table.Cell>
                  <Table.Cell>
                    <Price amount={order.total}/>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
            <hr/>
            <h3>{translate('comsa_sulu_shopping_cart.extra_details')}</h3>
            <Table skin="light">
              <Table.Header>
                <Table.HeaderCell>{translate('comsa_sulu_shopping_cart.paid_on')}</Table.HeaderCell>
                {orderProperties.map((p) => (
                  <Table.HeaderCell>{translate('properties.' + p)}</Table.HeaderCell>
                ))}
              </Table.Header>
              <Table.Body>
                <Table.Row>
                  <Table.Cell>{order.payment.paymentCompletedAt ? order.payment.paymentCompletedAt.replace('T', " ") : translate('comsa_sulu_shopping_cart.not_paid')}</Table.Cell>
                  {orderProperties.map((p) => (
                    <Table.Cell>{order[p].includes('T') ? order[p].replace('T', ' ') : order[p]}</Table.Cell>
                  ))}
                </Table.Row>
              </Table.Body>
            </Table>
          </Card>
        </Section>
        <Section label="Customer">
          <Grid>
            <Grid.Item className={orderStyles.contactInfo}>
              <Card>
                <h3>{translate('comsa_sulu_shopping_cart.contact_details')}</h3>
                <p><strong>{translate('comsa_sulu_shopping_cart.first_name')}:</strong> {order.cart.customer.firstName}</p>
                <p><strong>{translate('comsa_sulu_shopping_cart.last_name')}:</strong> {order.cart.customer.lastName}</p>
                <p><strong>{translate('comsa_sulu_shopping_cart.email')}:</strong> {order.cart.customer.email}</p>
                <p><strong>{translate('comsa_sulu_shopping_cart.phone')}:</strong> {order.cart.customer.phoneNumber}</p>
                <p><strong>{translate('comsa_sulu_shopping_cart.created_on')}:</strong> {order.cart.customer.createdAt}</p>
              </Card>
            </Grid.Item>
            <Grid.Item colSpan={6}>
              <Card>
                <h3>{translate('comsa_sulu_shopping_cart.billing_address')}</h3>
                <Address data={order.cart.customer.billingAddress}/>
              </Card>
            </Grid.Item>
            {order.cart.customer.shippingAddress &&
            <Grid.Item colSpan={6}>
              <Card>
                <h3>{translate('comsa_sulu_shopping_cart.shipping_address')}</h3>
                <Address data={order.cart.customer.shippingAddress}/>
              </Card>
            </Grid.Item>
            }
          </Grid>
        </Section>
        <Section label="Remarks">
          <Grid>
            <Grid.Item className={orderStyles.contactInfo}>
              <Card>
                <h3>{translate('comsa_sulu_shopping_cart.remarks')}</h3>
                <p>{order.note}</p>
              </Card>
            </Grid.Item>
          </Grid>
        </Section>
        {(order.cart.shippingMethod || order.cart.paymentMethod) &&
        <Section label="Shipment and Payment">
          <Grid>
            {order.cart.shippingMethod &&
            <Grid.Item colSpan={6}>
              <Card>
                <h3>{translate('comsa_sulu_shopping_cart.shipping')}</h3>
                <p><strong>{translate('comsa_sulu_shopping_cart.shipping_method')}: </strong> {order.cart.shippingMethod.name}</p>
                <Divider/>
                <h3>{translate('comsa_sulu_shopping_cart.address')}</h3>
                {
                  order.cart.customer.shippingAddress
                    ? <Address data={order.cart.customer.shippingAddress}/>
                    : <Address data={order.cart.customer.billingAddress}/>
                }
              </Card>
            </Grid.Item>
            }
            {order.cart.paymentMethod &&
            <Grid.Item colSpan={6}>
              <Card>
                <h3>{translate('comsa_sulu_shopping_cart.payment')}</h3>
                <p><strong>{translate('comsa_sulu_shopping_cart.payment_method')}</strong> {order.cart.paymentMethod.name}</p>
                {order.payment &&
                <div>
                  <p><strong>{translate('comsa_sulu_shopping_cart.amount')}:</strong> <Price amount={order.payment.amount}/></p>
                  <p><strong>{translate('comsa_sulu_shopping_cart.created_on')}:</strong> {order.payment.createdAt}</p>
                  <p><strong>{translate('comsa_sulu_shopping_cart.external_id')}:</strong> {order.payment.externalId}</p>
                  <p><strong>{translate('comsa_sulu_shopping_cart.payment_completed_at')}:</strong>
                    {order.payment.paymentCompletedAt ? order.payment.paymentCompletedAt.replace('T', ' ') : translate('comsa_sulu_shopping_cart.not_paid')}
                  </p>
                  <p><strong>{translate('comsa_sulu_shopping_cart.refunded')}: </strong>{order.payment.refunded ? translate('comsa_sulu_shopping_cart.yes') : translate('comsa_sulu_shopping_cart.no')}</p>
                </div>
                }
              </Card>
            </Grid.Item>
            }
          </Grid>
        </Section>
        }
      </div>
    )
  }
}

export default withToolbar(Order, function () {

  return {
    items: [
      {
        type: 'button',
        label: translate('comsa_sulu_shopping_cart.set_paid'),
        icon: 'su-check',
        onClick: () => {
          this.setPaid()
        },
      }
    ]
  };
});
