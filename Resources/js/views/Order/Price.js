import React from 'react';

type Props<T: string | number> = {
  amount: Number
};

function formatPrice(amount)
{
  const formatter = new Intl.NumberFormat('nl', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 2
  })

  return formatter.format(amount);
}

export default class Price extends React.Component<Props> {
  render() {
    const { amount } = this.props;
    const formattedAmount = formatPrice(amount);
    return (
      formattedAmount
    )
  }
}
