import React from 'react';
import type {ViewProps} from 'sulu-admin-bundle/containers/ViewRenderer';

type Props<T: string | number> = {
  data: Array
};

export default class Address extends React.Component<Props> {
  render() {
    const { data } = this.props;
    return (
      <address>
        {data.street} {data.streetNumber}<br/>
        {data.postal}, {data.city}<br/>
        {data.country}
      </address>
    )
  }
}
