const toggleShipping = document.getElementById('toggle-shipping');
if (toggleShipping) {
  const toggleShippingContainer = document.getElementById('toggle-shipping-container');
  toggleShipping.addEventListener('change', (e) => {
    toggleShippingContainer.style.display = e.target.checked ? 'block' : 'none';
  });
}

const toggleCart = document.querySelectorAll('.toggle-cart').forEach(toggle => {
  toggle.addEventListener('click', (e) => {
    e.preventDefault();
    //-- Find cart
    const smallCart = document.querySelector('.cart-small');

    const hidden = getComputedStyle(smallCart).display === 'none';
    smallCart.style.display = hidden ? 'block' : 'none';
  })
});
