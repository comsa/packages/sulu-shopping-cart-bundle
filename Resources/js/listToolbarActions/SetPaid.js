import {translate} from 'sulu-admin-bundle/utils';
import {AbstractListToolbarAction} from 'sulu-admin-bundle/views';
import symfonyRouting from 'fos-jsrouting/router';
import {action, observable} from "mobx";
import axios from "axios"

export default class SetPaid extends AbstractListToolbarAction {

  getToolbarItemConfig() {
    const {disable_for_empty_selection: disableForEmptySelection = true} = this.options;

    return {
      type: 'button',
      label: translate('comsa_sulu_shopping_cart.set_paid'),
      icon: 'su-check',
      onClick: this.handleClick,
    };
  }

  handleClick = () => {
    this.loading = true;

    let url = window.location.href;
    let split = url.split("/");
    let id = split[4];

    const response = axios.put(symfonyRouting.generate('comsa_sulu_shopping_cart.put_order_paid', {id: id }))

    window.location.reload();
  }


}
