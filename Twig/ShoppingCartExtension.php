<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Twig;

use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Manager\CartManager;
use Massive\Bundle\SearchBundle\Tests\Resources\TestBundle\Entity\Car;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Twig Extension for Shopping Cart
 * @package Comsa\SuluShoppingCart\Twig
 */
class ShoppingCartExtension extends AbstractExtension {

    private CartManager $cartManager;
    private TranslatorInterface $translator;

    public function __construct(
        CartManager $cartManager,
        TranslatorInterface $translator
    ) {
        $this->cartManager = $cartManager;
        $this->translator = $translator;
    }

    public function getFilters(): array {
        return [
            new TwigFilter("price", [$this, "renderPrice"])
        ];
    }

    public function getFunctions(): array {
        return [
            new TwigFunction("cart", [$this, "getCart"]),
            new TwigFunction("is_DateTime", [$this, "isDateTime"]),
            new TwigFunction("is_product_available", [$this, "isProductAvailable"])
        ];
    }

    public function renderPrice($price): string {
        if ((float) $price == 0) {
            return $this->translator->trans("comsa_sulu_shopping_cart.free");
        }
        return "€" . number_format($price, 2);
    }

    public function getCart(): Cart {
        return $this->cartManager->get();
    }

    public function isDateTime(Order $order, $property): bool {
        $functionName = "get" . ucfirst($property);

        if ($order->$functionName() instanceof \DateTime)  {
            return true;
        }

        return false;
    }

    public function isProductAvailable(Product $product): bool {
        if ($product->getTimeLimitedAvailability()) {
            $now = new \DateTime();

            if (!($now >= $product->getStartTimeAvailability() && $now <= $product->getEndTimeAvailability())) {
                return false;
            }
        }

        return true;
    }
}
