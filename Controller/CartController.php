<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller;

use Comsa\SuluShoppingCart\Entity\CartItem as BaseCartItem;
use App\Entity\CartItem;
use App\Entity\Order;
use Comsa\SuluShoppingCart\Entity\Cart;
use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Form\AddToCartType;
use Comsa\SuluShoppingCart\Form\OrderType;
use Comsa\SuluShoppingCart\Manager\CartManager;
use Comsa\SuluShoppingCart\Manager\OrderManager;
use Comsa\SuluShoppingCart\Repository\CartItemRepository;
use Comsa\SuluShoppingCart\Repository\OptionRepository;
use Comsa\SuluShoppingCart\Repository\ProductOptionRepository;
use Comsa\SuluShoppingCart\Repository\ProductRepository;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Frontend Cart Controller
 * @package Comsa\SuluShoppingCart\Controller
 */
class CartController extends AbstractController {

    private ProductRepository $productRepository;
    private SettingRepository $settingRepository;
    private OptionRepository $optionRepository;
    private CartItemRepository $cartItemRepository;
    private EntityManagerInterface $entityManager;
    private ProductOptionRepository $productOptionRepository;

    public function __construct(
        ProductRepository $productRepository,
        SettingRepository $settingRepository,
        OptionRepository $optionRepository,
        CartItemRepository $cartItemRepository,
        EntityManagerInterface $entityManager,
        ProductOptionRepository $productOptionRepository,
    ) {
        $this->productRepository = $productRepository;
        $this->settingRepository = $settingRepository;
        $this->optionRepository = $optionRepository;
        $this->cartItemRepository = $cartItemRepository;
        $this->entityManager = $entityManager;
        $this->productOptionRepository = $productOptionRepository;
    }

    public function addToCart(string $uuid, CartManager $cartManager): Response {
        $cartItem = new CartItem();
        $cartItem->setUuid($uuid);
        $cartManager->setCartItemData($cartItem);

        $product = $cartItem->getProduct();
        $productOptions = $product->getOptions();

        $options = [];
        foreach ($productOptions as $productOption) {
            $options[] = $productOption;
        }

        $form = $this->createForm(AddToCartType::class, $cartItem, [
            "product" => $product,
            "options" => $options
        ]);
        return $this->render('@SuluShoppingCart/add-to-cart.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function addToCartSubmit(Request $request, CartManager $cartManager): Response {
        /** @var Cart $cart */
        $cart = $cartManager->get();
        $submittedItem = $request->request->get("add_to_cart");
        $product = $this->productRepository->findOneByTitle($submittedItem["title"]);

        //-- Check which type is inside cart
        //-- If cartitem = single payment and new product = single payment --> allow - Else return flash message with notification
        if (count($cart->getItems()) > 0) {
            /** @var CartItem $item */
            foreach ($cart->getItems() as $item) {
                $cartProduct = $item->getProduct();
                //-- Exclude options
                if ($cartProduct) {
                    if ($cartProduct->getPaymentMethodType() === PaymentMethod::PAYMENT_METHOD_TYPE_SUBSCRIPTION && !$cartProduct->getPageId() === PaymentMethod::PAYMENT_METHOD_TYPE_SUBSCRIPTION) {
                        $this->addFlash('payFirst', 'Gelieve eerst het product via afbetaling in de winkelwagen af te ronden.');
                        return $this->redirect($request->headers->get('referer'));
                    } elseif ($cartProduct->getPaymentMethodType() !== $product->getPaymentMethodType()) {
                        $this->addFlash('payFirst', 'Gelieve eerst product(en) in uw winkelmand af te ronden.');
                        return $this->redirect($request->headers->get('referer'));
                    }
                }
            }
        }

        $cartItem = new CartItem();

        $baseMetaData = $this->entityManager->getClassMetadata(BaseCartItem::class)->getFieldNames();
        $metadata = $this->entityManager->getClassMetadata(CartItem::class)->getFieldNames();

        $chosenOptions = [];

        $cartItem->setTitle($submittedItem["title"]);
        $cartItem->setPrice(TypeConverter::stringToFloat($submittedItem["price"]));
        $cartItem->setQuantity(TypeConverter::stringToInt($submittedItem["quantity"]));

        foreach ($metadata as $property) {
            if (!in_array($property, $baseMetaData)) {
                if (isset($submittedItem[$property])) {
                    $functionName = "set" . ucfirst($property);
                    $cartItem->$functionName($submittedItem[$property]);
                    $cartItem->setCart($cart);
                }
            }
        }

        $cartItem->setProduct($product);
        $cartItem->setCart($cart);

        $existingOptions = $cartItem->getOptions();
        $newOptions = [];

        if ($product->getOptionSelectionType() === Option::OPTION_SELECTION_TYPE_MULTIPLE) {
            if (isset($submittedItem["options"]) && is_array($submittedItem["options"])) {
                foreach ($submittedItem["options"] as $optionId) {
                    $productOption = $this->productOptionRepository->find($optionId);
                    $cartItem->addOption($productOption->getOption());
                    $newOptions[] = $productOption->getOption();
                }
            }
        }
        else {
            if (isset($submittedItem["singleOption"]) && $submittedItem["singleOption"]) {
                $productOption = $this->productOptionRepository->find($submittedItem["singleOption"]);
                $cartItem->addOption($productOption->getOption());
                $newOptions[] = $productOption->getOption();
            }
        }

        foreach ($existingOptions as $existingOption) {
            if (!in_array($existingOption, $newOptions)) {
                $cartItem->removeOption($existingOption);
            }
        }

        $cart->addItem($cartItem);

        $cartManager->save($cart);

        //-- Redirect to cart controller
        return $this->redirectToRoute('comsa_sulu_shopping_cart');
    }

    public function index(CartManager $cartManager, TemplateAttributeResolverInterface $templateAttributeResolver, Request $request): Response {
        $baseCartItemProperties = ["id", "product", "uuid", "quantity", "title", "options", "price", "discr"];
        $customCartItemProperties = [];
        $metadata = $this->getDoctrine()->getManager()->getClassMetadata(CartItem::class)->getFieldNames();
        foreach ($metadata as $property) {
          if (!in_array($property, $baseCartItemProperties)) {
              $customCartItemProperties[] = $property;
          }
        }

        $cart = $cartManager->get();
        $data = $templateAttributeResolver->resolve([
            'cart' => $cart,
            'customCartItemProperties' => $customCartItemProperties,
            'backUrl' => $request->headers->get('referer')
        ]);
        return $this->render('@SuluShoppingCart/cart.html.twig', $data);
    }

    public function overview(Request $request, CartManager $cartManager, TemplateAttributeResolverInterface $templateAttributeResolver, OrderManager $orderManager): Response {
        $cart = $cartManager->get();

        $entityManager = $this->getDoctrine();

        $freeDateInputSetting = $this->settingRepository->findOneByKey(SettingEnum::KEYS_FREE_ORDER_DATE);

        if ($freeDateInputSetting->getValue() == "false") {
            $freeDateInput = false;
        }
        else {
            $freeDateInput = true;
        }

        $minDaysBeforeSetting = $this->settingRepository->findOneByKey(SettingEnum::KEYS_FREE_ORDER_DATE_DAYS_BEFORE);

        if ($minDaysBeforeSetting->getValue()!== 0 ) {
            $minDate = (new \DateTime())->modify("+{$minDaysBeforeSetting->getValue()} days")->setTime(10,00,00)->format("c");
        }
        else {
            $minDate = (new \DateTime())->setTime(10,00,00)->format("c");
        }

        $minDate = str_replace("+01:00", "", $minDate);
        $minDate = str_replace("+02:00", "", $minDate);

        $order = new Order();
        $form = $this->createForm(OrderType::class, $order, [
            "freeDateInput" => $freeDateInput,
            "minDate" => $minDate
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $order->setCart($cart);
            $orderManager->save($order);

            return $this->redirectToRoute('comsa_sulu_shopping_cart_handle_payment');
        }

        $baseCartItemProperties = ["id", "product", "uuid", "quantity", "title", "options", "price", "discr"];
        $customCartItemProperties = [];
        $metadata = $this->getDoctrine()->getManager()->getClassMetadata(CartItem::class)->getFieldNames();
        foreach ($metadata as $property) {
            if (!in_array($property, $baseCartItemProperties)) {
                $customCartItemProperties[] = $property;
            }
        }

        $data = $templateAttributeResolver->resolve([
            'cart' => $cart,
            'form' => $form->createView(),
            'customCartItemProperties' => $customCartItemProperties,
        ]);

        return $this->render('@SuluShoppingCart/overview.html.twig', $data);
    }

    public function deleteItem(int $id): Response {
        //-- Get item
        $cartItem = $this->cartItemRepository->find($id);
        $cartItem->getCart()->removeItem($cartItem);

        //-- Remove item
        $this->entityManager->remove($cartItem);
        $this->entityManager->flush();

        return $this->redirectToRoute('comsa_sulu_shopping_cart');
    }
}
