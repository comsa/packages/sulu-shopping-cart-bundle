<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller;

use Comsa\SuluShoppingCart\Entity\CartItem;
use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Event\OrderConfirmedEvent;
use Comsa\SuluShoppingCart\Event\OrderFulfilledEvent;
use Comsa\SuluShoppingCart\EventListener\OrderSubscriber;
use Comsa\SuluShoppingCart\Manager\OrderManager;
use Comsa\SuluShoppingCart\Repository\OrderRepository;
use Comsa\SuluShoppingCart\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Frontend Order Controller
 * @package Comsa\SuluShoppingCart\Controller
 */
class OrderController extends AbstractController {

    private EventDispatcherInterface $eventDispatcher;
    private ParameterBagInterface $parameterBag;
    private EntityManagerInterface $em;
    private ProductRepository $productRepository;
    private OrderRepository $orderRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher, ParameterBagInterface $parameterBag, EntityManagerInterface $em, ProductRepository $productRepository, OrderRepository $orderRepository) {
        $this->eventDispatcher = $eventDispatcher;
        $this->parameterBag = $parameterBag;
        $this->em = $em;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
    }

    public function thanks(OrderManager $orderManager, TemplateAttributeResolverInterface $templateAttributeResolver, EntityManagerInterface $eventDispatcher): Response {
        $order = $orderManager->get();
        $cart = $order->getCart();
        $cartItems = $cart->getItems();
        $paymentMethod = $order->getCart()->getPaymentMethod();

        if ($paymentMethod->getType() === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_mollie');
        } else {
            $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_default');
        }

        $parameters = ['order' => $order];

        if (isset($typeHandler)) {
            $parameters = array_merge($parameters, $typeHandler->getAdditionalTemplateData($order));
        }

        $data = $templateAttributeResolver->resolve($parameters);

//        $this->confirmOrder($order);
//        $this->fulfillOrder($order);


        if ($paymentMethod->getType() === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            return $this->render("@SuluShoppingCart/paymentMethods/mollie.html.twig", $parameters);
        } else {
            return $this->render('@SuluShoppingCart/paymentMethods/default.html.twig', $parameters);
        }

    }

    public function export(Request $request): Response
    {
        $productId = $request->query->get("id");

        $product = $this->productRepository->find($productId);
        $orders = $this->orderRepository->findAll();

        $ordersToPrint = new ArrayCollection();

        /** @var Order $order */
        foreach ($orders as $order) {
            $cartItems = $order->getCart()->getItems();

            /** @var CartItem $item */
            foreach ($cartItems as $item) {
                if ($item->getProduct() === $product) {
                    $ordersToPrint->add($order);
                }
            }
        }

        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $sheet->setTitle("Bestellingen");

        $sheet->setCellValue("A1",  "Naam");
        $sheet->setCellValue("B1", "Email");
        $sheet->setCellValue("C1", "Opties");
        $sheet->setCellValue("D1", "Betaald op");

        $sheet->getStyle("A1:D1")->applyFromArray([
           "fill" => [
               "fillType" => Fill::FILL_SOLID,
               "startColor" => [
                   "argb" => "FFFF00"
               ]
           ]
        ]);

        $x = 3;

        foreach ($ordersToPrint as $order) {
            if (!$order->getPayment()->isRefunded()) {
                $sheet->setCellValue("A" . $x, $order->getCart()->getCustomer()->getFullName());
                $sheet->setCellValue("B" . $x, $order->getCart()->getCustomer()->getEmail());
                $sheet->setCellValue("C" . $x, $this->formatOptions($order->getCart()->getItems()));
                $sheet->setCellValue("D" . $x, $order->getPayment()->getPaymentCompletedAt() ? $order->getPayment()->getPaymentCompletedAt()->format("d-m-Y H:i:s") : "");

                $x++;
            }
        }

        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);

        $sheet->getStyle("A1:D" . $x)->getAlignment()->setWrapText(true);

        $writer = new Xlsx($spreadSheet);
        $fileName = sprintf("Bestellingen: %s.xlsx", str_replace("/", "-", $product->getTitle()));
        $tempFile = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($tempFile);

        return $this->file($tempFile, $fileName);
    }

    private function formatOptions(Collection $cartItems): string {
        $string = null;
        /** @var CartItem $cartItem */
        foreach ($cartItems as $cartItem) {
            /** @var Option $option */
            foreach ($cartItem->getOptions() as $option) {
                $string .= $option === $cartItem->getOptions()->last() ? $option->getTitle() . ' x ' . $cartItem->getQuantity() : $option->getTitle() . ' x ' . $cartItem->getQuantity() . '\n';
            }
        }

        if ($string) {
            $string = str_replace('\n', "\n", $string);
            return $string;
        } else {
            return "";
        }
    }

    private function confirmOrder(Order $order): void {
        $event = new OrderConfirmedEvent($order);
        $this->eventDispatcher->dispatch($event, OrderConfirmedEvent::NAME);
    }
}
