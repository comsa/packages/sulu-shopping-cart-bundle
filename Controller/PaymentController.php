<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller;

use Comsa\SuluShoppingCart\Entity\Payment;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Manager\CartManager;
use Comsa\SuluShoppingCart\Manager\OrderManager;
use Comsa\SuluShoppingCart\PaymentMethods\Interfaces\PaymentMethodInterface;
use Comsa\SuluShoppingCart\PaymentMethods\MollieType;
use Comsa\SuluShoppingCart\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Frontend Payment Controller
 * @package Comsa\SuluShoppingCart\Controller
 */
class PaymentController extends AbstractController {

    private PaymentRepository $paymentRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(PaymentRepository $paymentRepository, EntityManagerInterface $entityManager) {
        $this->paymentRepository = $paymentRepository;
        $this->entityManager = $entityManager;
    }

    public function select(Request $request, CartManager $cartManager, TemplateAttributeResolverInterface $templateAttributeResolver): Response {
        $cart = $cartManager->get();

        $form = $this->createFormBuilder()
            ->add('paymentMethod', EntityType::class, [
                'class' => PaymentMethod::class,
                "query_builder" => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder("pm")
                        ->where("pm.hidden = false");
                },
                'expanded' => true,
                'label' => "comsa_sulu_shopping_cart.payment_method"
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cart->setPaymentMethod($form->get('paymentMethod')->getData());
            $cartManager->save($cart);

            return $this->redirectToRoute('comsa_sulu_shopping_cart_overview');
        }

        $data = $templateAttributeResolver->resolve([
            'cart' => $cart,
            'form' => $form->createView()
        ]);

        return $this->render('@SuluShoppingCart/select-payment.html.twig', $data);
    }

    public function handle(OrderManager $orderManager): Response {
        $order = $orderManager->get();
        $paymentMethod = $order->getCart()->getPaymentMethod();

        $paymentMethod = $order->getCart()->getPaymentMethod()->getType();

        if ($paymentMethod === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_mollie');
        } else {
            $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_default');
        }

        //-- Create payment
        $payment = new Payment();
        if ($order->getCart()->getItems()[0]->getProduct()->getPaymentMethodType() === PaymentMethod::PAYMENT_METHOD_TYPE_SUBSCRIPTION) {
            $payment->setPaymentMethodType(PaymentMethod::PAYMENT_METHOD_TYPE_SUBSCRIPTION);
        } else {
            $payment->setPaymentMethodType(PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT);
        }

        $payment->setOrder($order);
        $payment->setAmount($order->getCart()->getTotal());

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $typeHandler->handlePayment($payment);
    }

    public function handleRedirect(OrderManager $orderManager): Response {
        $paymentMethod = $orderManager->get()->getCart()->getPaymentMethod()->getType();

        if ($paymentMethod === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_mollie');
        } else {
            $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_default');
        }

        $order = $orderManager->get();
        return $typeHandler->afterPayment($order);
    }

    public function webhook(Request $request): Response {
        /** @var MollieType $typeHandler */
        $typeHandler = $this->container->get('comsa_sulu_shopping_cart_payment_method_mollie');

        //-- Get id
        $id = $request->request->get('id');

        if (!$id) {
            $id = $request->request->get('subscriptionId');
        }

        $payment = $this->getPayment($id);

        if ($typeHandler->isRefunded($payment)) {
            $payment->refund();
            $this->entityManager->flush();
            return new Response("Payment has been refunded", Response::HTTP_OK);
        }

        if (!$request->request->get("subscriptionId")) {
            $isPaid = $typeHandler->checkPayment($payment);
        } else {
            $isPaid = $typeHandler->checkSubscription($payment);
        }

        //-- Only return if it's not a subscription
        if (!$request->request->get("subscriptionId")) {
            if (!$isPaid) {
                return new Response('Status has not changed', Response::HTTP_OK);
            }
        }

        //-- If single payment just check payment status and send mail
        //-- else check if recurring_times = null --> send mail else just update
        if ($payment->getPaymentMethodType() === PaymentMethod::PAYMENT_METHOD_TYPE_SINGLE_PAYMENT) {
            //-- Update paid status
            $payment->setPaid();
            $this->getDoctrine()->getManager()->flush();
            $typeHandler->afterSuccessWebhook($payment);
        } else {
            if (is_null($payment->getRecurringTimes())) {
                $payment->setRecurringTimes(1);
                $typeHandler->afterSuccessWebhook($payment);
            } else {
                $recurringTimes = $payment->getRecurringTimes() + 1;
                $payment->setRecurringTimes($recurringTimes);
            }

            $payment->setPaid();
            $this->entityManager->flush();
        }

        return new Response('Status has been updated', Response::HTTP_OK);
    }

    private function getPayment(string $id): Payment {
        /** @var Payment $payment */
        $payment = $this->paymentRepository->findOneByExternalId($id);

        if (!$payment) {
            /** @var Payment $payment */
            $payment = $this->paymentRepository->findOneByExternalId($id);
        }

        if (!$payment) {
            $payment = $this->paymentRepository->findOneByExternalSubscriptionId($id);
        }

        return $payment;
    }
}
