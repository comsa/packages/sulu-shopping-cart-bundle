<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller;

use Comsa\SuluShoppingCart\Entity\Address;
use Comsa\SuluShoppingCart\Entity\Customer;
use Comsa\SuluShoppingCart\Factory\AddressFactory;
use Comsa\SuluShoppingCart\Form\CustomerType;
use Comsa\SuluShoppingCart\Manager\CartManager;
use Comsa\SuluShoppingCart\Repository\AddressRepository;
use Comsa\SuluShoppingCart\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Mollie\Api\MollieApiClient;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Frontend Customer Controller
 * @package Comsa\SuluShoppingCart\Controller
 */
class CustomerController extends AbstractController {

    private AddressRepository $addressRepository;
    private CustomerRepository $customerRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        AddressRepository $addressRepository,
        CustomerRepository $customerRepository,
        EntityManagerInterface $entityManager,
    ) {
        $this->addressRepository = $addressRepository;
        $this->customerRepository = $customerRepository;
        $this->entityManager = $entityManager;
    }

    public function create(Request $request, TemplateAttributeResolverInterface $templateAttributeResolver, EntityManagerInterface $entityManager, CartManager $cartManager): Response{
        $cart = $cartManager->get();
        $customer = new Customer();
        $customer->setBillingAddress((new Address())->setCountry('BE'));
        $customer->setShippingAddress((new Address())->setCountry('BE'));
        $form = $this->createForm(CustomerType::class, $customer);
        $data = $templateAttributeResolver->resolve([
            'form' => $form->createView()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //-- Get all formdata

            /** @var Customer $formData */
            $formData = $form->getData();

            $firstname = $formData->getFirstName();
            $lastname = $formData->getLastName();
            $email = $formData->getEmail();
            $phoneNumber = $formData->getPhoneNumber();
            $billingAddress = $formData->getBillingAddress();
            $shippingAddress = $formData->getShippingAddress();

            //-- Check if billingAddress already exists
            $existingBillingAddress = $this->addressRepository->findAddress(
                street: $billingAddress->getStreet(),
                streetNumber: $billingAddress->getStreetNumber(),
                postal: $billingAddress->getPostal(),
                city: $billingAddress->getCity(),
                country: $billingAddress->getCountry()
            );

            //-- Check if customer already exists based on existing billing address - else continue to creating a new customer
            if ($existingBillingAddress) {
                $existingCustomer = $this->customerRepository->findDuplicate(
                    firstname: $firstname,
                    lastname: $lastname,
                    email: $email,
                    phone: $phoneNumber,
                    billingAddress: $existingBillingAddress
                );

                /*
                 If customer exists check if shippingAddress is filled, if filled get shippingAddress compare to customer - else continue to creating a new customer
                 If customer shipping null, shipping is filled --> add shipping (add new Address or take existing)
                 If customer shipping null, shipping is null --> use existing customer
                 If customer shipping filled, shipping is null --> set shipping as null
                */
                if ($existingCustomer) {
                    if (!$existingCustomer->getShippingAddress() && $shippingAddress->getStreet()) {
                        $existingShippingAddress = $this->addressRepository->findAddress(
                          street: $shippingAddress->getStreet(),
                          streetNumber: $shippingAddress->getStreetNumber(),
                          postal: $shippingAddress->getPostal(),
                          city: $shippingAddress->getCity(),
                          country: $shippingAddress->getCountry()
                        );

                        if ($existingShippingAddress) {
                            $existingCustomer->setShippingAddress($existingShippingAddress);
                        } else {
                            $existingCustomer->setShippingAddress(AddressFactory::create(
                                street: $shippingAddress->getStreet(),
                                streetNumber: $shippingAddress->getStreetNumber(),
                                postal: $shippingAddress->getPostal(),
                                city: $shippingAddress->getCity(),
                                country: $shippingAddress->getCountry()
                            ));
                        }
                    } else {
                        $existingCustomer->setShippingAddress(null);
                    }

                    $cart->setCustomer($existingCustomer);
                    $cartManager->save($cart);

                    return $cartManager->nextStep(1);
                }
            }


            //-- Create customer
            /** @var Customer $customer */
            $customer = $form->getData();

            //-- Check if billingAddress exists - set existing address as billing address
            if ($existingBillingAddress) {
                $customer->setBillingAddress($existingBillingAddress);
            }

            //-- Filter out shipping address if empty
            if (
                !$customer->getShippingAddress()->getCity() ||
                !$customer->getShippingAddress()->getPostal() ||
                !$customer->getShippingAddress()->getStreet() ||
                !$customer->getShippingAddress()->getStreetNumber()
            ) {
                $customer->setShippingAddress(null);
            }
            $customer->setMollieId($this->createMollieCustomer($customer));

            $this->getDoctrine()->getManager()->persist($customer);
            $cart->setCustomer($customer);
            $cartManager->save($cart);

            return $cartManager->nextStep(1);
        }

        return $this->render('@SuluShoppingCart/create-customer.html.twig', $data);
    }

    private function createMollieCustomer(Customer $customer): string {
        $mollie = new MollieApiClient();
        $mollie->setApiKey($this->container->getParameter('comsa_sulu_shopping_mollie_api_key'));
        $mollieCustomer = $mollie->customers->create([
           "name" => $customer->getFirstName() . ' ' . $customer->getLastName(),
            "email" => $customer->getEmail()
        ]);

        return $mollieCustomer->id;
    }
}
