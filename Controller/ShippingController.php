<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller;

use Comsa\SuluShoppingCart\Entity\ShippingMethod;
use Comsa\SuluShoppingCart\Manager\CartManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Frontend Shipping Controller
 * @package Comsa\SuluShoppingCart\Controller
 */
class ShippingController extends AbstractController {
    public function select(
        Request $request,
        CartManager $cartManager,
        TemplateAttributeResolverInterface $templateAttributeResolver,
        EntityManagerInterface $entityManager
    ): Response {
        $cart = $cartManager->get();

        $form = $this->createFormBuilder()
            ->add('shippingMethod', EntityType::class, [
                'class' => ShippingMethod::class,
                "query_builder" => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder("pm")
                        ->where("pm.hidden = false")
                    ;
                },
                'expanded' => true,
                'label' => "comsa_sulu_shopping_cart.shipping_method"
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cart->setShippingMethod($form->get('shippingMethod')->getData());
            $cartManager->save($cart);

            return $cartManager->nextStep(3);
        }

        $data = $templateAttributeResolver->resolve(['cart' => $cart, 'form' => $form->createView()]);
        return $this->render('@SuluShoppingCart/select-shipping.html.twig', $data);
    }
}
