<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller\Admin;

use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Setting;
use Comsa\SuluShoppingCart\Entity\ShippingMethod;
use Comsa\SuluShoppingCart\Enum\SettingEnum;
use Comsa\SuluShoppingCart\Factory\PaymentMethodFactory;
use Comsa\SuluShoppingCart\Factory\ShippingMethodFactory;
use Comsa\SuluShoppingCart\Repository\PaymentMethodRepository;
use Comsa\SuluShoppingCart\Repository\SettingRepository;
use Comsa\SuluShoppingCart\Repository\ShippingMethodRepository;
use Comsa\SuluShoppingCart\Service\PaymentMethodService;
use Comsa\SuluShoppingCart\Service\SettingService;
use Comsa\SuluShoppingCart\Service\ShippingMethodService;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use JMS\Serializer\SerializationContext;
use Mollie\Api\Resources\PaymentLink;
use Ramsey\Collection\Set;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListBuilderInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Configures the REST API for Setting
 * @package Comsa\SuluShoppingCart\Controller\Admin
 */
class SettingController extends AbstractRestController implements ClassResourceInterface
{
    private ViewHandlerInterface $viewHandler;
    private DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private SettingService $settingService;
    private PaymentMethodService $paymentMethodService;
    private ShippingMethodService $shippingMethodService;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        SettingService $settingService,
        PaymentMethodService $paymentMethodService,
        ShippingMethodService $shippingMethodService
    )
    {
        $this->viewHandler = $viewHandler;
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->settingService = $settingService;
        $this->paymentMethodService = $paymentMethodService;
        $this->shippingMethodService = $shippingMethodService;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $this->getLocale($request);

        if (!$locale) {
            $locale = "nl";
        }

        $listBuilder = $this->doctrineListBuilderFactory->create(Setting::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Setting::RESOURCE_KEY);
        $listBuilder->where($fieldDescriptors["title"], SettingEnum::TITLES_FREE_ORDER_DATE_DAYS_BEFORE, ListBuilderInterface::WHERE_COMPARATOR_UNEQUAL);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listBuilder->execute(),
            "settings",
            $request->get("_route"),
            $request->query->all(),
            $listBuilder->getCurrentPage(),
            $listBuilder->getLimit(),
            $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        $setting = $this->settingService->getRepository()->find($id);

        if (!$setting) {
            throw new NotFoundHttpException();
        }

        $formValueName = match ((string) $setting->getKey()) {
            SettingEnum::KEYS_PAYMENT_METHOD => SettingEnum::FORM_KEY_PAYMENT_METHOD,
            SettingEnum::KEYS_SHIPPING_METHOD => SettingEnum::FORM_KEY_SHIPPING_METHOD,
            SettingEnum::KEYS_ORDER_COMPLETED_TEXT => SettingEnum::FORM_KEY_ORDER_COMPLETED_TEXT,
            SettingEnum::KEYS_IBAN => SettingEnum::FORM_KEY_DEFAULT,
            SettingEnum::KEYS_BIC => SettingEnum::FORM_KEY_DEFAULT,
            SettingEnum::KEYS_EMAIL => SettingEnum::FORM_KEY_EMAIL,
            SettingEnum::KEYS_FREE_ORDER_DATE => SettingEnum::FORM_KEY_FREE_ORDER_DATE,
        };

        if ($setting->getKey() === SettingEnum::KEYS_PAYMENT_METHOD) {
            $paymentMethods = $this->paymentMethodService->getRepository()->findAll();
            if (!empty($paymentMethods)) {
                foreach ($paymentMethods as $paymentMethod) {
                    switch ($paymentMethod->getType()) {
                        case SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE:
                            $titleMollie = $paymentMethod->getName();
                            $priceMollie = $paymentMethod->getPrice();
                            break;
                        case SettingEnum::VALUES_PAYMENT_METHOD_BANK:
                            $titleBank = $paymentMethod->getName();
                            $priceBank = $paymentMethod->getPrice();
                            break;
                        case SettingEnum::VALUES_PAYMENT_METHOD_CASH:
                            $titleCash = $paymentMethod->getName();
                            $priceCash = $paymentMethod->getPrice();
                            break;
                    }
                }
            }
        } elseif ($setting->getKey() === SettingEnum::KEYS_SHIPPING_METHOD) {
            $shippingMethods = $this->shippingMethodService->getRepository()->findAll();
            if (!empty($shippingMethods)) {
                foreach ($shippingMethods as $shippingMethod) {
                    switch ($shippingMethod->getType()) {
                        case SettingEnum::VALUES_SHIPPING_METHOD_DELIVER:
                            $titleDeliver = $shippingMethod->getName();
                            $priceDeliver = $shippingMethod->getPrice();
                            break;
                        case SettingEnum::VALUES_SHIPPING_METHOD_PICKUP:
                            $titlePickup = $shippingMethod->getName();
                            $pricePickup = $shippingMethod->getPrice();
                            break;
                    }
                }
            }
        } elseif ($setting->getKey() === SettingEnum::KEYS_FREE_ORDER_DATE) {
            $minDaysBeforeSetting = $this->settingService->getRepository()->findOneBy([
                "key" => SettingEnum::KEYS_FREE_ORDER_DATE_DAYS_BEFORE
            ]);
            $minDaysBeforeValue = $minDaysBeforeSetting->getValue();
        }

        $response = new JsonResponse();
        $response->setData([
            "id" => $setting->getId(),
            "title" => $setting->getTitle(),
            "key" => $setting->getKey(),
            $formValueName => $setting->getValue(),
            SettingEnum::FORM_KEY_NAME_MOLLIE => isset($titleMollie) ? $titleMollie : null,
            SettingEnum::FORM_KEY_PRICE_MOLLIE => isset($priceMollie) ? $priceMollie : null,
            SettingEnum::FORM_KEY_NAME_BANK => isset($titleBank) ? $titleBank : null,
            SettingEnum::FORM_KEY_PRICE_BANK => isset($priceBank) ? $priceBank : null,
            SettingEnum::FORM_KEY_NAME_CASH => isset($titleCash) ? $titleCash : null,
            SettingEnum::FORM_KEY_PRICE_CASH => isset($priceCash) ? $priceCash : null,
            SettingEnum::FORM_KEY_NAME_DELIVER => isset($titleDeliver) ? $titleDeliver : null,
            SettingEnum::FORM_KEY_PRICE_DELIVER => isset($priceDeliver) ? $priceDeliver : null,
            SettingEnum::FORM_KEY_NAME_PICKUP => isset($titlePickup) ? $titlePickup : null,
            SettingEnum::FORM_KEY_PRICE_PICKUP => isset($pricePickup) ? $pricePickup : null,
            SettingEnum::FORM_KEY_FREE_ORDER_DAYS_BEFORE => isset($minDaysBeforeValue) ? (string) $minDaysBeforeValue : null
        ]);

        return $response;
    }

    public function putAction(int $id, Request $request): Response
    {
        $setting = $this->settingService->getRepository()->find($id);

        if (!$setting) {
            throw new NotFoundHttpException();
        }

        switch ($setting->getKey()) {
            case SettingEnum::KEYS_PAYMENT_METHOD:
                if (is_array($request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD))) {
                    $value = implode(" ,", $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD));
                } else {
                    $value = $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD);
                }

                $this->checkPaymentMethods($request);
                break;
            case SettingEnum::KEYS_SHIPPING_METHOD:
                if (is_array($request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD))) {
                    $value = implode(" ,", $request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD));
                } else {
                    $value = $request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD);
                }

                $this->checkShippingMethods($request);
                break;
            case SettingEnum::KEYS_ORDER_COMPLETED_TEXT:
                $value = $request->request->get(SettingEnum::FORM_KEY_ORDER_COMPLETED_TEXT);
                break;
            case SettingEnum::KEYS_EMAIL:
                $value = $request->request->get(SettingEnum::FORM_KEY_EMAIL);
                break;
            case SettingEnum::KEYS_FREE_ORDER_DATE:
                $this->saveMinDaysBefore($request);
                $value = $request->request->get(SettingEnum::FORM_KEY_FREE_ORDER_DATE);
                break;
            default:
                $value = $request->request->get(SettingEnum::FORM_KEY_DEFAULT);
        }

        $this->settingService->update($setting, [
            "value" => $value
        ]);

        return $this->handleView($this->view($setting));
    }

    private function checkPaymentMethods(Request $request) {
        $paymentMethods = $this->paymentMethodService->getRepository()->findAll();

        $oldPaymentMethods = [];

        if (is_array($request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD))) {
            $newPaymentMethods = $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD);
        }
        else {
            if ($request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD)) {
                $newPaymentMethods = explode(" ,", $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD));
            } else {
                $newPaymentMethods = [];
            }
        }

        //-- Check which payment method(s) are removed
        foreach ($paymentMethods as $paymentMethod) {
            $oldPaymentMethods[] = $paymentMethod->getType();
            if (!in_array($paymentMethod->getType(), $newPaymentMethods)) {
                $this->paymentMethodService->delete($paymentMethod);
            }
        }

        //-- Check which payment method(s) are added
        foreach ($newPaymentMethods as $paymentMethod) {
            $name = match ($paymentMethod) {
                SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE => $request->request->get(SettingEnum::FORM_KEY_NAME_MOLLIE),
                SettingEnum::VALUES_PAYMENT_METHOD_BANK => $request->request->get(SettingEnum::FORM_KEY_NAME_BANK),
                SettingEnum::VALUES_PAYMENT_METHOD_CASH => $request->request->get(SettingEnum::FORM_KEY_NAME_CASH)
            };

            $price = match ($paymentMethod) {
                SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE => $request->request->get(SettingEnum::FORM_KEY_PRICE_MOLLIE),
                SettingEnum::VALUES_PAYMENT_METHOD_BANK => $request->request->get(SettingEnum::FORM_KEY_PRICE_BANK),
                SettingEnum::VALUES_PAYMENT_METHOD_CASH => $request->request->get(SettingEnum::FORM_KEY_PRICE_CASH)
            };

            if (!in_array($paymentMethod, $oldPaymentMethods)) {
                $this->paymentMethodService->create([
                    "name" => $name,
                    "type" => $paymentMethod,
                    "price" => TypeConverter::stringToFloat((string) $price)
                ]);
            }  else {
                $this->paymentMethodService->update(
                    $this->paymentMethodService->getRepository()->findOneBy([
                       "type" => $paymentMethod
                    ]),
                    [
                    "name" => $name,
                    "price" => TypeConverter::stringToFloat((string) $price)
                    ]
                );

                $this->paymentMethodService->update(
                    $this->paymentMethodService->getRepository()->findOneBy([
                        "type" => $paymentMethod
                    ]),
                    [
                        "name" => $name,
                        "price" => $price
                    ]
                );
            }
        }
    }

    private function checkShippingMethods(Request $request) {
        $shippingMethods = $this->shippingMethodService->getRepository()->findAll();

        $oldShippingMethods = [];

        if (is_array($request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD)))  {
            $newShippingMethods = $request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD);
        }
        else  {
            if ($request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD)) {
                $newShippingMethods = explode(" ," , $request->request->get(SettingEnum::FORM_KEY_SHIPPING_METHOD));
            } else {
                $newShippingMethods = [];
            }
        }

        //-- Check which payment method(s) are removed
        foreach ($shippingMethods as $shippingMethod)  {
            $oldShippingMethods[] = $shippingMethod->getType();
            if (!in_array($shippingMethod->getType(), $newShippingMethods))  {
                $this->shippingMethodService->delete($shippingMethod);
            }
        }

        //-- Check which shippingmethod(s) are added
        foreach ($newShippingMethods as $shippingMethod)  {
            $name = match ($shippingMethod) {
                SettingEnum::VALUES_SHIPPING_METHOD_DELIVER => $request->request->get(SettingEnum::FORM_KEY_NAME_DELIVER),
                SettingEnum::VALUES_SHIPPING_METHOD_PICKUP => $request->request->get(SettingEnum::FORM_KEY_NAME_PICKUP),
            };

            $price = match ($shippingMethod) {
                SettingEnum::VALUES_SHIPPING_METHOD_DELIVER => $request->request->get(SettingEnum::FORM_KEY_PRICE_DELIVER),
                SettingEnum::VALUES_SHIPPING_METHOD_PICKUP => $request->request->get(SettingEnum::FORM_KEY_PRICE_PICKUP),
            };

            if (!in_array($shippingMethod, $oldShippingMethods))  {
                $this->shippingMethodService->create([
                    "name" => $name,
                    "type" => $shippingMethod,
                    "price" => $price
                ]);
            }  else  {
                $this->shippingMethodService->update(
                    $this->shippingMethodService->getRepository()->findOneBy([
                        "type" => $shippingMethod
                    ]),
                    [
                        "name" => $name,
                        "price" => $price
                    ]
                );
            }
        }
    }

    private function saveMinDaysBefore(Request $request)
    {
        $setting = $this->settingService->getRepository()->findOneBy([
            "key" => SettingEnum::KEYS_FREE_ORDER_DATE_DAYS_BEFORE
        ]);
        $this->settingService->update($setting, [
            "value" => $request->request->get(SettingEnum::FORM_KEY_FREE_ORDER_DATE) === "no" ? "0" : (string) $request->request->get(SettingEnum::FORM_KEY_FREE_ORDER_DAYS_BEFORE)
        ]);
    }
}
