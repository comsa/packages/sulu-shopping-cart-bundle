<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller\Admin;

use Comsa\SuluShoppingCart\Entity\TaxCategory;
use Comsa\SuluShoppingCart\Factory\TaxCategoryFactory;
use Comsa\SuluShoppingCart\Repository\TaxCategoryRepository;
use Comsa\SuluShoppingCart\Service\TaxCategoryService;
use Comsa\SuluShoppingCart\Utility\TypeConverter;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @RouteResource("tax-category")
 */
class TaxCategoryController extends AbstractRestController implements ClassResourceInterface {

    private DoctrineListBuilderFactoryInterface $factory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private TaxCategoryService $service;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        TaxCategoryService $service
    ) {
        $this->factory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->service = $service;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request) : Response {
        $listBuilder = $this->factory->create(TaxCategory::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(TaxCategory::RESOURCE_KEY);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
          $listBuilder->execute(),
          "tax_categories",
          $request->get("_route"),
          $request->query->all(),
          $listBuilder->getCurrentPage(),
          $listBuilder->getLimit(),
          $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response {
        /** @var TaxCategory $taxCategory */
        $taxCategory = $this->service->getRepository()->find($id);

        if (!$taxCategory) {
            throw new NotFoundHttpException();
        }

        $taxCategory->setPercentage($taxCategory->getPercentage() * 100);

        return $this->handleView($this->view($taxCategory));
    }

    public function postAction(Request $request): Response {
        $taxCategory = $this->service->create([
           "title" => $request->request->get("title"),
           "percentage" => TypeConverter::stringToPercentage((string) $request->request->get("percentage"))
        ]);

        return $this->handleView($this->view($taxCategory));
    }

    public function putAction(int $id, Request $request): Response {
        /** @var TaxCategory $taxCategory */
        $taxCategory = $this->service->getRepository()->find($id);

        if (!$taxCategory) {
            throw new NotFoundHttpException();
        }

        $this->service->update($taxCategory, [
            "title" => $request->request->get("title"),
            "percentage" => TypeConverter::stringToPercentage((string) $request->request->get("percentage"))
        ]);

        return $this->handleView($this->view($taxCategory->setPercentage($taxCategory->getPercentage() * 100 )));
    }

    public function deleteAction(int $id): Response {
        $taxCategory = $this->service->getRepository()->find($id);

        if (!$taxCategory) {
            throw new NotFoundHttpException();
        }

        $this->service->delete($taxCategory);

        return $this->handleView($this->view());
    }
}
