<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller\Admin;

use Comsa\SuluShoppingCart\Admin\OrderAdmin;
use Comsa\SuluShoppingCart\Entity\Order;
use Comsa\SuluShoppingCart\Repository\OrderRepository;
use Comsa\SuluShoppingCart\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\ListRestHelperInterface;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Configures the REST API for Order
 * @package Comsa\SuluShoppingCart\Controller\Admin
 */
class OrderController extends AbstractRestController implements ClassResourceInterface {
    private DoctrineListBuilderFactoryInterface $factory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private OrderService $orderService;

    public function __construct(
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        ViewHandlerInterface $viewHandler,
        OrderService $orderService,
    ) {
        parent::__construct($viewHandler);
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->factory = $doctrineListBuilderFactory;
        $this->restHelper = $restHelper;
        $this->orderService = $orderService;
    }

    public function cgetAction(Request $request): Response {
        $listBuilder = $this->factory->create(Order::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Order::RESOURCE_KEY);

        if ($request->get("id")) {
            $id = $request->get("id");
            if (isset($fieldDescriptors["productId"])) {
                $listBuilder->where($fieldDescriptors["productId"], $id);
                $listBuilder->where($fieldDescriptors["refunded"], false);
                $listBuilder->distinct($fieldDescriptors["id"]);
            }
        }

        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $list = $listBuilder->execute();

        foreach ($list as &$item) {
            $order = $this->orderService->getRepository()->find($item["id"]);
            $item['total'] = '€' . number_format($order->getTotal(), 2);
        }

        $representation = new ListRepresentation(
            $list,
            OrderAdmin::ORDER_LIST_KEY,
            $request->get('_route'),
            $request->query->all(),
            $listBuilder->getCurrentPage(),
            $listBuilder->getLimit(),
            $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id): Response {
        $order = $this->orderService->getRepository()->find($id);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($order));
    }

    public function deleteAction(int $id): Response {
        $order = $this->orderService->getRepository()->find($id);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        $this->orderService->delete($order);

        return $this->handleView($this->view());
    }

    public function putPaidAction(int $id): Response {
        $order = $this->orderService->getRepository()->find($id);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        $this->orderService->setPaid($order);

        return $this->handleView($this->view($order));
    }
}
