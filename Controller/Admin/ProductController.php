<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller\Admin;

use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use Sulu\Component\Content\Types\ResourceLocator\Strategy\ResourceLocatorStrategyPool;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Comsa\SuluShoppingCart\Event\Product\ProductCreatedEvent;
use Comsa\SuluShoppingCart\Event\Product\ProductUpdatedEvent;
use Comsa\SuluShoppingCart\Event\Product\ProductDeletedEvent;

/**
 * Configures the REST API for Product
 * @package Comsa\SuluShoppingCart\Controller\Admin
 */
class ProductController extends AbstractRestController implements ClassResourceInterface
{
    private DoctrineListBuilderFactoryInterface $factory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private EntityManagerInterface $entityManager;
    private DocumentManagerInterface $documentManager;
    private ResourceLocatorStrategyPool $resourceLocatorStrategyPool;
    private WebspaceManagerInterface $webspaceManager;
    private ProductService $service;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        EntityManagerInterface $entityManager,
        DocumentManagerInterface $documentManager,
        ResourceLocatorStrategyPool $resourceLocatorStrategyPool,
        WebspaceManagerInterface $webspaceManager,
        ProductService $service,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->factory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->entityManager = $entityManager;
        $this->documentManager = $documentManager;
        $this->resourceLocatorStrategyPool = $resourceLocatorStrategyPool;
        $this->webspaceManager = $webspaceManager;
        $this->service = $service;
        $this->eventDispatcher = $eventDispatcher;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $listBuilder = $this->factory->create(Product::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Product::RESOURCE_KEY);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $listBuilder->addSearchField($fieldDescriptors["title"]);
        $listBuilder->addSearchField($fieldDescriptors["price"]);

        $representation= new ListRepresentation(
            $listBuilder->execute(),
            'products',
            $request->get("_route"),
            $request->query->all(),
            $listBuilder->getCurrentPage(),
            $listBuilder->getLimit(),
            $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        /** @var Product $entity */
        $entity = $this->service->getRepository()->find($id);

        if (!$entity) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($entity->__toArray()));
    }

    public function postAction(Request $request): Response
    {
        $webspaces = $this->webspaceManager->getWebspaceCollection();
        $webspaceKey = $webspaces->toArray()["webspaces"][0]["key"];

        $product = $this->createProduct($request);

        $this->eventDispatcher->dispatch(
            new ProductCreatedEvent($product, $request->getLocale(), $webspaceKey),
            ProductCreatedEvent::NAME
        );

        return $this->handleView($this->view($product));
    }

    public function putAction(int $id, Request $request): Response
    {
        $webspaces = $this->webspaceManager->getWebspaceCollection();
        $webspaceKey = $webspaces->toArray()["webspaces"][0]["key"];

        /** @var Product $product */
        $product = $this->service->getRepository()->find($id);
        if (!$product) {
            throw new NotFoundHttpException();
        }

        $this->updateProduct($product, $request);
        $this->eventDispatcher->dispatch(
            new ProductUpdatedEvent($product, $request->getLocale(), $webspaceKey),
            ProductUpdatedEvent::NAME
        );

        return $this->handleView($this->view($product));
    }

    public function deleteAction(int $id, Request $request): Response
    {
        /** @var Product $product */
        $product = $this->service->getRepository()->find($id);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        $webspaces = $this->webspaceManager->getWebspaceCollection();
        $webspaceKey = $webspaces->toArray()["webspaces"][0]["key"];

        $this->service->delete($product);
        $this->eventDispatcher->dispatch(
          new ProductDeletedEvent($product),
          ProductDeletedEvent::NAME
        );

        return $this->handleView($this->view());
    }

    private function createProduct(Request $request): Product {
        return $this->service->create($request->request->all());
    }

    private function updateProduct(Product $product, Request $request): void {
        $this->service->update($product, $request->request->all());
    }
}
