<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Controller\Admin;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Service\OptionService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use JMS\Serializer\SerializationContext;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OptionController extends AbstractRestController implements ClassResourceInterface {

    private DoctrineListBuilderFactoryInterface $factory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private OptionService $optionService;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        OptionService $optionService
    ) {
        $this->factory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->optionService = $optionService;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response {
        $listBuilder = $this->factory->create(Option::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Option::RESOURCE_KEY);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listBuilder->execute(),
            'options',
            $request->get("_route"),
            $request->query->all(),
            $listBuilder->getCurrentPage(),
            $listBuilder->getLimit(),
            $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response {
        $option = $this->optionService->getRepository()->find($id);
        if (!$option)  {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($option));
    }

    public function postAction(Request $request): Response {
        $option = $this->optionService->create($request->request->all());
        $this->optionService->save($option);

        return $this->handleView($this->view($option));
    }

    public function putAction(int $id, Request $request): Response {
        $option = $this->optionService->getRepository()->find($id);

        if (!$option)  {
            throw new NotFoundHttpException();
        }

        $this->optionService->update($request->request->all());

        return $this->handleView($this->view($option));
    }

    public function deleteAction(int $id): Response {
        $option = $this->optionService->getRepository()->find($id);

        if (!$option) {
            throw new NotFoundHttpException("Option does not exist.");
        }

        $this->optionService->delete($option);

        return $this->handleView($this->view());
    }
}
