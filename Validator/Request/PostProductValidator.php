<?php

declare(strict_types=1);

namespace Comsa\SuluShoppingCart\Validator\Request;

use Comsa\SuluShoppingCart\Entity\Option;
use Comsa\SuluShoppingCart\Entity\PaymentMethod;
use Comsa\SuluShoppingCart\Entity\Product;
use Comsa\SuluShoppingCart\Enum\ProductEnum;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class PostProductValidator {
    public static function validate(array $request) {
        $validator = Validation::createValidator();

        $constraint = new Assert\Collection([
            "title" => new Assert\NotBlank(),
            "price" => new Assert\GreaterThan(0),
            "paymentMethodType" => new Assert\Choice(PaymentMethod::PAYMENT_METHOD_TYPE_ARRAY),
            "unit" => new Assert\Choice(ProductEnum::UNIT_ARRAY),
            "optionSelectionType" => new Assert\Choice(Option::OPTION_SELECTION_TYPES)
        ]);

        $constraint->allowExtraFields = true;

        $violations = $validator->validate($request, $constraint);

        return $violations;
    }
}
